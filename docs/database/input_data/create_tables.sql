CREATE TABLE Antigen (
  ag_name VARCHAR(25) PRIMARY KEY,
  ag_alias VARCHAR(15) UNIQUE,
  ag_group VARCHAR(2) NOT NULL,
  ag_broad VARCHAR(10),
  ag_Bw VARCHAR(3)
);

CREATE TABLE Allele (
  allele_name VARCHAR(15) PRIMARY KEY,
  allele_group VARCHAR(4) NOT NULL,
  EU_frequency FLOAT,
  allele_seq TEXT NOT NULL
);

CREATE TABLE Has_allele (
  ag_name VARCHAR(25),
  allele_name VARCHAR(15),
  PRIMARY KEY (ag_name, allele_name),
  FOREIGN KEY (ag_name) REFERENCES Antigen(ag_name),
  FOREIGN KEY (allele_name) REFERENCES Allele(allele_name)
);

CREATE TABLE Eplet (
  eplet_id SMALLINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  eplet_name VARCHAR(20) NOT NULL,
  eplet_residues VARCHAR(100) NOT NULL,
  ellipro_score VARCHAR(12),
  confirmed_status BOOLEAN,
  eplet_source VARCHAR(100),
  addition_date DATE NOT NULL
);

CREATE TABLE Has_eplet (
  allele_name VARCHAR(15),
  eplet_id SMALLINT UNSIGNED,
  PRIMARY KEY (allele_name, eplet_id),
  FOREIGN KEY (allele_name) REFERENCES Allele(allele_name),
  FOREIGN KEY (eplet_id) REFERENCES Eplet(eplet_id)
);

CREATE TABLE Exp_structure (
  exp_structure_id VARCHAR(4) PRIMARY KEY,
  ag_name VARCHAR(25) NOT NULL,
  source_name VARCHAR(10) NOT NULL,
  structure_date DATE NOT NULL,
  resolution FLOAT NOT NULL,
  comment_on_repair TEXT NOT NULL,
  path2file VARCHAR(255) NOT NULL,
  FOREIGN KEY (ag_name) REFERENCES Antigen(ag_name)
);

CREATE TABLE Mod_structure (
  mod_structure_id VARCHAR(100) PRIMARY KEY,
  ag_name VARCHAR(25) NOT NULL,
  model_parameters TEXT NOT NULL,
  model_date DATE NOT NULL,
  path2file VARCHAR(255) NOT NULL,
  FOREIGN KEY (ag_name) REFERENCES Antigen(ag_name)
);

CREATE TABLE Has_template (
  exp_structure_id VARCHAR(4),
  mod_structure_id VARCHAR(100),
  PRIMARY KEY (exp_structure_id, mod_structure_id),
  FOREIGN KEY (exp_structure_id) REFERENCES Exp_structure(exp_structure_id),
  FOREIGN KEY (mod_structure_id) REFERENCES Mod_structure(mod_structure_id)
);

CREATE TABLE MD_run (
  md_run_id VARCHAR(100) PRIMARY KEY,
  exp_structure_id VARCHAR(4),
  mod_structure_id VARCHAR(100),
  run_date DATE NOT NULL,
  run_parameters TEXT NULL,
  run_duration TINYINT UNSIGNED NOT NULL,
  nber_frames SMALLINT UNSIGNED NOT NULL,
  path2dcdfile VARCHAR(255) NOT NULL,
  path2psffile VARCHAR(255) NOT NULL,
  used_in_visu BOOLEAN,
  path2pdbfile VARCHAR(255),
  FOREIGN KEY (exp_structure_id) REFERENCES Exp_structure(exp_structure_id),
  FOREIGN KEY (mod_structure_id) REFERENCES Mod_structure(mod_structure_id)
);

CREATE TABLE SASA (
  sasa_id VARCHAR(100) PRIMARY KEY,
  md_run_id VARCHAR(100) NOT NULL,
  sasa_method VARCHAR(255) NOT NULL,
  sasa_parameters TEXT NOT NULL,
  sasa_date DATE NOT NULL,
  path2file VARCHAR(255) NOT NULL,
  FOREIGN KEY (md_run_id) REFERENCES MD_run(md_run_id)
);

CREATE TABLE Patch_run (
  patch_run_id VARCHAR(100) PRIMARY KEY,
  md_run_id VARCHAR(100) NOT NULL,
  path_method VARCHAR(255) NOT NULL,
  patch_parameters TEXT NOT NULL,
  center_aa VARCHAR(4) NOT NULL,
  radius TINYINT UNSIGNED NOT NULL,
  patch_date DATE NOT NULL,
  path2files VARCHAR(255) NOT NULL,
  FOREIGN KEY (md_run_id) REFERENCES MD_run(md_run_id)
);

CREATE TABLE Coeff_representation (
  coeff_id VARCHAR(100) PRIMARY KEY,
  md_run_id VARCHAR(100),
  patch_run_id VARCHAR(100),
  coeff_method VARCHAR(255) NOT NULL,
  coeff_parameters TEXT NOT NULL,
  coeff_date DATE NOT NULL,
  coeff_type VARCHAR(15) NOT NULL,
  coeff_dimension SMALLINT UNSIGNED NOT NULL,
  path2file VARCHAR(255) NOT NULL,
  FOREIGN KEY (md_run_id) REFERENCES MD_run(md_run_id),
  FOREIGN KEY (patch_run_id) REFERENCES Patch_run(patch_run_id)
);
