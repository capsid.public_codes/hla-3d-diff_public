include .env

up:	## Creates and start the application
	@docker compose up -d

stop: ## Stops the application
	@docker compose stop

importdb:	## Imports the default database
	@docker compose exec -T db mysql -uroot -p$(MYSQL_ROOT_PASSWORD) $(DATABASE_NAME) < $(DEFAULT_DATABASE_DUMP)

dumpdb: ## Saves the database
	@docker compose exec -T db mysqldump -uroot -p$(MYSQL_ROOT_PASSWORD) -x -F $(DATABASE_NAME) > $(DATABASE_DUMP)

restoredb: ## Restores a database previously saved
	@docker compose exec -T db mysql -uroot -p$(MYSQL_ROOT_PASSWORD) $(DATABASE_NAME) < $(DATABASE_DUMP)
