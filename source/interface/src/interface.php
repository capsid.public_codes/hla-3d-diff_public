<!-- HLA-3D-Diff visualisation interface -->

<?php
include "mysql.php";
?>

<!DOCTYPE html>
<html>
<head>
    <title>HLA-3D-Diff - interface de visualisation</title>
    <meta charset="UTF-8"/>
    <link rel="stylesheet" href="css/interface.css">
    <script src="https://kit.fontawesome.com/37a22deb3b.js"></script> <!-- icons -->

    <script src="NGL/ngl.js"></script> <!-- NGL -->
</head>

<body>

  <div id="viewport"></div>

  <div id="menu">
  </div>

  <div id="sequence_nav">
    <div id="sequence_names"> </div>
    <div>
      <div id="positions"> </div>
      <div id="sequence_1"> </div>
      <div id="sequence_2"> </div>
    </div>
  </div>

  <div id="general_parameters"> </div>

  <div id="info_panel"> </div>
  <button id="info_panel_button" class="fa_button fas fa-chevron-left" title="Hide"></button>

  <div id="general_labels"> </div>

  <div id="gradients_labels"> </div>

</body>

  <?php

    $structure1 = htmlspecialchars($_GET['structure1']);
    $structure2 = htmlspecialchars($_GET['structure2']);

    /**
     * Given an antigen, returns relevant associated data after querying the database.
     * @param string $structure_name Antigen name.
     * @return array Data associated with the antigen. Keys: 'name', 'group', 'path2pdbfile', 'path2dcdfile', 'path2psffile', 'run_duration', 'nber_frames', 'sequenceA', 'sequenceB', 'epletsA', 'epletsB', 'sasa'.
     * 'group': 'A'/'B'/'C'/'DR'/'DQ'/'DP'. 'sequenceA' is the sequence of the alpha chain, 'sequenceB' is either the sequence of the beta chain (DQ/DP antigens) or set to "".
     * 'epletsA' is an array listing the eplets of the alpha chain. See get_data_associated_with_allele() for more information. 'epletsB' is either an array listing the eplets of the beta chain (DQ/DP antigens) or set to "".
     * 'sasa' gives the path to the file with RSASA values if it exists and is set to null otherwise.
     */
    function get_structure_data($structure_name) {
      $data = array();
      $data['name'] = $structure_name;

      if (in_array(substr($structure_name, 0, 1), ["A", "B", "C"])) {
        $data['group'] = substr($structure_name, 0, 1);
      } else {
        $data['group'] = substr($structure_name, 0, 2);
      }

      $connection = set_db_connection();

      $query_md_run = "SELECT path2pdbfile, path2dcdfile, path2psffile, run_duration, nber_frames, SASA.path2file FROM MD_run
         LEFT JOIN Exp_structure ON MD_run.exp_structure_id = Exp_structure.exp_structure_id
         LEFT JOIN Mod_structure ON MD_run.mod_structure_id = Mod_structure.mod_structure_id
         LEFT JOIN SASA ON MD_run.md_run_id = SASA.MD_run_id
         WHERE used_in_visu = 1 AND (Exp_structure.ag_name = '$structure_name'
           OR Mod_structure.ag_name = '$structure_name')";
      $result_md_run = mysqli_query($connection, $query_md_run);
      $result_md_run = mysqli_fetch_assoc(mysqli_query($connection, $query_md_run));
      $data['path2pdbfile'] = './data/'.$result_md_run['path2pdbfile']; // ./data/ is linked to the shared folder defined by the environment variable 'DATA_DIRECTORY' (docker-compose.yml)
      $data['path2dcdfile'] = './data/'.$result_md_run['path2dcdfile'];
      $data['path2psffile'] = './data/'.$result_md_run['path2psffile'];
      $data['run_duration'] = $result_md_run['run_duration'];
      $data['nber_frames'] = $result_md_run['nber_frames'];
      if ($result_md_run['path2file'] != null) {
        $data['sasa'] = './data/'.$result_md_run['path2file'];
      } else {
        $data['sasa'] = null;
      }

      if (in_array($data['group'], ["DQ", "DP"])) { // two chains
        $chains = explode("-", $structure_name);
        $chainA = get_data_associated_with_allele($chains[1], $connection);
        $chainB = get_data_associated_with_allele($chains[0], $connection);
        $data['sequenceA'] = $chainA['seq'];
        $data['sequenceB'] = $chainB['seq'];
        $data['epletsA'] = $chainA['eplets'];
        $data['epletsB'] = $chainB['eplets'];
      } else {  // one chain
        $chainA = get_data_associated_with_allele($structure_name, $connection);
        $data['sequenceA'] = $chainA['seq'];
        $data['epletsA'] = $chainA['eplets'];
        $data['sequenceB'] = "";
        $data['epletsB'] = "";
      }

      mysqli_close($connection);
      return $data;
    }

    /**
     * Given an allele, queries the database and returns its sequence and associated eplets.
     * @param string $allele_name
     * @param mysqli $connection
     * @return array Sequence and eplets. Keys: 'seq', 'eplets'.
     * 'eplets': array. Keys: 'eplet_name', 'eplet_residues', 'ellipro_score' (initials, 'x' if none), 'confirmed_status' ('C' or 'x').
     */
    function get_data_associated_with_allele($allele_name, $connection) {
      $data = array();
      $query_seq = "SELECT allele_seq FROM Allele WHERE allele_name = '$allele_name'";
      $result_seq = mysqli_fetch_assoc(mysqli_query($connection, $query_seq));
      $data['seq'] = $result_seq['allele_seq'];
      $eplets = array();
      $query_eplets = "SELECT eplet_name, eplet_residues, ellipro_score, confirmed_status FROM Eplet
        JOIN Has_eplet ON Eplet.eplet_id = Has_eplet.eplet_id
        WHERE allele_name = '$allele_name'";
      $result_eplets = mysqli_query($connection, $query_eplets);
      while ($eplet = mysqli_fetch_assoc($result_eplets)) {
        $ellipro_score = $eplet['ellipro_score'];
        if ($ellipro_score == "NULL" || $ellipro_score == "") {
          $ellipro_score = "x";
        } else {
          $ellipro_score = preg_replace('/[^A-Z]/', '', $ellipro_score); // Selection of the uppercase letters
          // to only keep the initials ('High' -> 'H', 'Very Low' -> 'VL', etc.)
        }
        $confirmed_status = $eplet['confirmed_status'];
        if ($confirmed_status == 1) {
          $confirmed_status = "C";
        } else {
          $confirmed_status = "x";
        }
        $eplets[] = [$eplet['eplet_name'], $ellipro_score, $confirmed_status, $eplet['eplet_residues']];
      }
      $data['eplets'] = $eplets;
      return $data;
    }

    // Gather data for the first antigen
    $structure1_data = get_structure_data($structure1);

    // If a second antigen was selected, gather data and superpose structures
    if ($structure2 != "none") {
      $structure2_data = get_structure_data($structure2);

      // Creation of two temporary files (a .pdb and a .psf), that will be deleted once they've been uploaded by NGL

      $output_name = str_replace(':', '-', str_replace('*', '-', $structure2))."_aligned_to_".str_replace(':', '-', str_replace('*', '-', $structure1));

      // The superposition is performed using VMD
      // VMD is running in a separate Docker container: request with curl
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, 'vmd:8080/align_with_vmd');
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"path_psf1\":\"".$structure1_data['path2psffile']."\",\"path_dcd1\":\"".$structure1_data['path2dcdfile']."\",\"path_psf2\":\"".$structure2_data['path2psffile']."\",\"path_dcd2\":\"".$structure2_data['path2dcdfile']."\",\"output_name\":\"".$output_name."\"}");

      $headers = array();
      $headers[] = 'Content-Type: application/json';
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

      $result = curl_exec($ch);
      if (curl_errno($ch)) {
          echo 'Error:' . curl_error($ch);
      }
      curl_close($ch);

      $structure2_data['path2pdbfile'] = './tmp/'.$output_name.".pdb"; // ./tmp/ is linked to the shared folder defined by the environment variable 'TMP_DIRECTORY' (docker-compose.yml)
      $structure2_data['path2dcdfile'] = './tmp/'.$output_name.".dcd";

    } else {
      $structure2_data = NULL;
    }
    
  ?>

  <script>
    const antigen1 = <?php echo json_encode($structure1_data);?>;
    const antigen2 = <?php echo json_encode($structure2_data);?>;
    console.log("Antigen1:", antigen1);
    console.log("Antigen2:", antigen2);

    
  </script>

  <script src="js/interface.js" type="module">  </script>

</html>
