/**
* @file Constants concerning HLA antigens and their 3D models.
*/

/**
* Last modelled residue (the transmembrane region is not included in 3D models.)
*/
export const end_simulated_sequence = {
  A: 284,
  B: 285,
  C: 285,
  DR: 198,
  DQ: 193,
  DQA1: 193,
  DQB1: 198,
  DP: 194,
  DPA1: 194,
  DPB1: 196
};

/**
* Insertions / deletions in the mature protein sequence of alleles documented in the database.
* Used to correct the positions of eplets and for the alignment of sequences.
* Indels given in reference to the reference allele for a given gene.
* Currently, only one deletion (in some DQA1) has an impact on the position of eplets. Its gestion is directly implemented throughout the code as it's found in several antigens.
*/
export const indels = {
  'B*73:01': [1, 296],  // insertion of 1 amino acid on position 296
  'C*17:01': [6, 300], // insertion of 6 aa on position 300
  'DQB1*06:01': [8, 226] // insertion of 8 aa on position 226
  /* The 1 aa deletion on position 56 in all DQA1*02, DQA1*04, DQA1*05 and DQA1*06 is directly dealt with in the different functions. */
}

/**
* Approximate positions used for labelling the structures.
* peptideBindingCleftPosition: targets the peptide binding cleft.
* transmembraneRegionPosition: targets the end of the modelled structure / the beginning of the transmembrane region.
*/
export const peptideBindingCleftPositionClassI = "60-80:A 140-170:A";
export const peptideBindingCleftPositionClassII = "60-80:A 55-85:B";
export const transmembraneRegionPositionClassI = "253:A";
export const transmembraneRegionPositionClassII = "193:B";
