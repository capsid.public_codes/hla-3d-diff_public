/**
* @file Manages Selection objects.
* One selection object = one selection 'block' in the interface menu = one possible 3D selection with consistent style (representations and colors).
* @module Selection
* @module ResiduesSelection
* @module EpletsSelection
* @module PatchSelection
*/

import {RepresentationWindow, ColorWindow} from './style.js';
import {isButtonActive, showMessage, toggleVisibilityButton, removeItemFromArrayOnce} from './misc.js';
import {getColorSummary} from './colors.js';
import {indels} from './constants.js';

class Selection {
  parent; /* Structure object */
  structure;  /* "main" or "superposed" */
  antigen;

  /* A Selection object corresponds to a selection 'block' in the interface menu, with 4 main buttons. */
  div;  /* selection 'block' in the interface menu */
  showButton; /* for the user to toggle the visibility of the selected 3D elements */
  representationButton; /* for the user to manage the representations of the selected 3D elements */
  representationWindow; /* corresponding RepresentationWindow object */
  colorButton; /* for the user to manage the color parameters of the selected 3D elements */
  colorWindow; /* corresponding ColorWindow object */
  deleteButton; /* for the user to delete the selection 'block' + the eventual selected 3D elements */

  currentSelection = "not protein"; /* current selection, in NGL syntax */
  // set to "not protein" as an empty string would select everything
  previousSelection = "";

  reprDict = []; /* array of NGL representation components, corresponds to currently selected and displayed representations (e.g. a 'cartoon' representation and a 'licorice' representation) */
  currentColorParameters; /* current color parameters, in NGL syntax (list) */

  infoDiv; /* designated line in the info panel, to display a summary of the selection */

  /**
  * Generates a Selection object, which enables the user to select elements of the parent 3D structure.
  * @param {Structure} structure Parent Structure object.
  */
  constructor(structure) {
    this.parent = structure;
    this.antigen = this.parent.antigen;
    this.structure = this.parent.id;

    // Generates the selection 'block' in the interface menu
    this.generateHTMLElement();

    // Adds a div in the info panel, to display a summary of the selection
    this.infoDiv = document.createElement('div');
    this.infoDiv.setAttribute("class", "info_panel_selection_details");
    this.parent.infoDiv.append(this.infoDiv);

    // Adds the eventlisteners for the 4 buttons
    let self = this;
    // showButton
    this.showButton.addEventListener("click", function(){
      self.toggleSelectionVisibility(false);
    });
    // deleteButton: confirmation (double click) needed
    this.deleteButton.addEventListener("click", function(){ // Asks for confirmation
      this.classList.add('deleteInfo');
      let button = this;
      setTimeout(function () {
        button.classList.remove('deleteInfo');
      }, 1500)
    });
    this.deleteButton.addEventListener("dblclick", function(){self.delete()});
    // representationButton: creation of a RepresentationWindow object
    this.representationWindow = new RepresentationWindow(self, self.representationButton, self.structure);
    this.representationButton.addEventListener("click", function(){self.representationWindow.toggleWindowVisibility()});
    // colorButton: creation of a ColorWindow object
    this.colorWindow = new ColorWindow(self, self.colorButton, self.structure);
    this.colorButton.addEventListener("click", function(){self.colorWindow.toggleWindowVisibility()});
  }

  get div() {
    return this.div();
  }

  /**
  * Creates the selection 'block' in the interface menu (HTML only).
  */
  generateHTMLElement() {
    // Main div
    this.div = document.createElement('div');
    this.div.setAttribute("class", "selection_block");

    // Input div - specific to the type of selection block
    this.inputDiv = document.createElement('div');
    this.inputDiv.setAttribute("class", "selection_block_input_div");

    // 4 buttons - shared by all types of selection blocks
    var buttonsDiv = document.createElement('div');
    buttonsDiv.setAttribute("class", "selection_block_buttons_div");

    // Generate a button to toggle the visibility of the selection
    this.showButton = document.createElement('button');
    this.showButton.className = "fa_button fas fa-eye";

    // Generate a button to change the representation(s) of the selection
    this.representationButton = document.createElement('button');
    this.representationButton.className = "fa_button fas fa-project-diagram";

    // Generate a button to change the color of the selection
    this.colorButton = document.createElement('button');
    this.colorButton.className = "fa_button fas fa-palette";

    // Generate a button to delete the selection block
    this.deleteButton = document.createElement('button');
    this.deleteButton.className = "fa_button fas fa-trash-alt";

    this.div.append(this.inputDiv);
    buttonsDiv.append(this.showButton);
    buttonsDiv.insertAdjacentHTML("beforeend", "<span class=\"vertical_line_2\"> </span>");
    buttonsDiv.append(this.representationButton);
    buttonsDiv.insertAdjacentHTML("beforeend", "<span class=\"vertical_line_2\"> </span>");
    buttonsDiv.append(this.colorButton);
    buttonsDiv.insertAdjacentHTML("beforeend", "<span class=\"vertical_line_2\"> </span>");
    buttonsDiv.append(this.deleteButton);
    this.div.append(buttonsDiv);
  }

  /**
  * Deletes the selection block in the interface menu and the possible 3D selection and details.
  */
  delete() {
    this.div.remove();

    // Removes details about the selection in the info panel + unselects residues in the sequence panel
    this.previousSelection= this.currentSelection;
    this.currentSelection = "not protein";
    this.updateSelection();

    // So that if there is a scale displayed, it hides it
    this.colorWindow.desactivateButtons();

    // Removes 3D selection
    for(let reprName in this.reprDict) {
      this.reprDict[reprName].dispose();
    }
  }

  /**
  * Updates the 3D selection and the details according to the value of currentSelection.
  */
  updateSelection() {
    // Updates 3D selection
    if (this.currentSelection == "") { this.currentSelection = "not protein"}; // an empty string would select everything
    for(let reprName in this.reprDict) {
      // For some reason, simply updating the 'sele' parameter with the setParameters() function doesn't work
      // so we delete all representations and recreate them
      var opacityValue = this.reprDict[reprName].getParameters().opacity;
      this.reprDict[reprName].dispose();
      this.reprDict[reprName] = this.parent.NGLstructure.addRepresentation(reprName, {sele: this.currentSelection, opacity: opacityValue, opaqueBack: false});
      this.reprDict[reprName].setParameters(this.currentColorParameters);
    }
    // Updates details (in info panel and in sequence panel)
    this.updateResidues();
    this.updateDetails();
  }

  /**
  * Updates the details about the selection in the info panel according to the value of currentSelection.
  */
  updateDetails() {
    this.infoDiv.innerHTML = "";
    if (this.currentSelection != "not protein" && this.isSetToVisible()) {
      this.infoDiv.insertAdjacentHTML("beforeend", "<span class=\"fas fa-mouse-pointer info_panel_list_icon\"> </span>");
      this.infoDiv.insertAdjacentHTML("beforeend", this.getSelectionDetails());
      this.infoDiv.append(getColorSummary(this.currentColorParameters)); // a dot or the color scheme name with the corresponding gradient
    }
  }

  /**
  * Selects and unselects residues in the sequence panel according to the values of currentSelection and previousSelection.
  */
  updateResidues() {
    // Residues included in a selection are highlighted in the sequence panel.
    // For each residue included in the selection, the class 'selected_residue' is added to the corresponding residue component in the sequence panel.
    // As different selections can overlap, a residue component in the sequence panel might have 'selected_residues' several times in its className.
    // This way, it will stay highlighted as long as one selection remains.
    let self = this;
    if (this.previousSelection != "" && this.previousSelection != "not protein") {
      // For each residue of the previous selection, deletes a "selected_residue" from the className of the corresponding residue component
      this.parent.NGLstructure.structure.getView( new NGL.Selection( this.previousSelection ) ).eachResidue(function(r) {
        if (self.antigen.group == "DQ" || self.antigen.group == "DP") { // Two chains in the sequence panel
          if (r.chain.chainname == "B") {
            document.getElementById(self.structure+"_chainB_"+r.resno).className = removeItemFromArrayOnce(document.getElementById(self.structure+"_chainB_"+r.resno).className.split(" "), "selected_residue").join(" ");
          } else {
            document.getElementById(self.structure+"_chainA_"+r.resno).className = removeItemFromArrayOnce(document.getElementById(self.structure+"_chainA_"+r.resno).className.split(" "), "selected_residue").join(" ");
          }
        } else { // One chain (the sequence of the second chain isn't displayed for A, B, C, DR antigens)
          document.getElementById(self.structure+"_"+r.resno).className = removeItemFromArrayOnce(document.getElementById(self.structure+"_"+r.resno).className.split(" "), "selected_residue").join(" ");;
        }
      });
    }
    if (this.currentSelection != "not protein" && this.isSetToVisible()) {
      // For each residue in the new selection, adds a "selected_residue" to the className of the corresponding residue component
      this.parent.NGLstructure.structure.getView( new NGL.Selection( this.currentSelection ) ).eachResidue(function(r) {
        if (self.antigen.group == "DQ" || self.antigen.group == "DP") {
          if (r.chain.chainname == "B") {
            document.getElementById(self.structure+"_chainB_"+r.resno).className += " selected_residue";
          } else {
            document.getElementById(self.structure+"_chainA_"+r.resno).className += " selected_residue";
          }
        } else {
          if (r.chain.chainname != "B") {
            document.getElementById(self.structure+"_"+r.resno).className += " selected_residue";
          }
        }
      });
    }
  }

  /**
  * Adds a new representation for the selection.
  * @param {str} reprName Name of the representation as understood by NGL, e.g. 'cartoon'.
  * @param {float} opacityValue Opacity. 1 is fully opaque, 0 is fully transparent.
  */
  addRepr(reprName, opacityValue) {
    this.reprDict[reprName] = this.parent.NGLstructure.addRepresentation(reprName, {sele: this.currentSelection, opacity: opacityValue, opaqueBack: false});
    this.reprDict[reprName].setParameters(this.currentColorParameters);
  }

  /**
  * Deletes a representation of the selection.
  * @param {str} reprName Name of the representation as understood by NGL, e.g. 'cartoon'.
  */
  deleteRepr(reprName) {
    this.reprDict[reprName].dispose();
    delete this.reprDict[reprName];
  }

  /**
  * Modifies the opacity of a representation of the selection.
  * @param {str} reprName One of the current representations for the selection. Name of the representation as understood by NGL, e.g. 'cartoon'.
  * @param {float} opacityValue Opacity. 1 is fully opaque, 0 is fully transparent.
  */
  changeReprOpacity(reprName, opacityValue) {
    this.reprDict[reprName].setParameters({sele: this.currentSelection, opacity: opacityValue});
  }

  /**
  * Modifies the color parameters of the selection.
  * @param {object} colorParameters Color parameters in the NGL syntax.
  * @param {boolean} differentColorScheme True if the user selected new color parameters (default), false otherwise.
  * 'differentColorScheme' is for when the structure is colored by RSASA, as the color parameters change with every frame, but the summary doesn't need to be updated.
  */
  changeColor(colorParameters, differentColorScheme = true) {
    this.currentColorParameters = colorParameters;
    for(var repr in this.reprDict) {
        this.reprDict[repr].setParameters(colorParameters);
    }
    if (differentColorScheme) {
      this.updateDetails();
    }
  }

  /**
  * Re-creates all representations for the selection as they were.
  * Is used because, as far as I know, you can't set what goes 'above' what in the NGL viewport. So recreating a selection is a way to make it visible again when hidden by a larger selection.
  */
  rebuildRepresentations() {
    for(var reprName in this.reprDict) {
      var reprParams = this.reprDict[reprName].getParameters();
      this.reprDict[reprName].dispose();
      this.reprDict[reprName] = this.parent.NGLstructure.addRepresentation(reprName, reprParams);
    }
  }

  /**
  * Hides or shows the 3D selection.
  * @param {boolean} calledFromParent Indicates whether the user changed the visibility of the selection block (0) or of the parent structure (1).
  */
  toggleSelectionVisibility(calledFromParent) {
    if (!calledFromParent) {
      toggleVisibilityButton(this.showButton);
    }
    if ((calledFromParent && this.isSetToVisible()) || !calledFromParent) {
      // If the parent structure is hidden, and the selection is already hidden, then do nothing
      for(let reprName in this.reprDict) {
        this.reprDict[reprName].toggleVisibility();
      }
    }
  }

  /**
  * Indicates whether the selection is set to visible, independant of whether or not the selection is visible.
  * (The selection can be hidden because the parent structure was set to hidden.)
  * @return {boolean} 1 for set to visible, 0 for set to hidden.
  */
  isSetToVisible() {
    if (this.showButton.classList.contains("fa-eye")) {
      return true;
    } else {
      return false;
    }
  }
}


export class ResiduesSelection extends Selection {
  selection; /* Input field */

  /**
  * Generates a ResiduesSelection object, which enables the user to select residues or other elements according to the NGL selection syntax.
  */
  constructor(structure) {
    super(structure);
    // this.div.setAttribute("id", "residues_selection_"+this.id);

    // Generates a text field for user input + a submit button
    this.inputDiv.classList.add("residues_input_div");
    this.selection = document.createElement('input');
    this.selection.setAttribute("class", "residues_input");
    this.selection.setAttribute("type", "text");
    this.selection.setAttribute("placeholder", "Positions");
    this.inputDiv.append(this.selection);
    var button = document.createElement('button');
    button.setAttribute("class", "validate_selection");
    button.innerHTML = "OK";
    this.inputDiv.append(button);

    // Generates a button for the user to get information about the NGL selection syntax
    var info = document.createElement('button');
    info.className = "fa_button far fa-question-circle";
    this.inputDiv.append(info);

    // Adds eventlisteners
    let self = this;
    button.addEventListener("click", function() {
      self.onValidation();
    });
    this.selection.addEventListener('keypress', function (e) {
      // Hitting 'enter' after inputting a selection amounts to clicking on the submit button
      if (e.key === 'Enter') {
        self.onValidation();
      }
    });
    info.addEventListener("click", function() {
      self.showResiduesSelectionInfo(this);
    });
  }

  /**
  * Updates the selection.
  */
  onValidation() {
    this.previousSelection = this.currentSelection;
    this.currentSelection = this.selection.value;
    this.updateSelection();
  }

  /**
  * Displays help about the NGL selection syntax.
  * @param {HTMLElement} triggerButton Button on which the user clicks to display the help dialog.
  */
  showResiduesSelectionInfo(triggerButton) {
    var message = "<b><u>NGL selection language</u></b> <br> <br>" +
        "<b>1 2 100</b> list of residues <br>"+
        "<b>3-40</b> range of residues <br>"+
        "<b>:A</b> to specify the chain <i>e.g. <b>10:A</b></i> <br> <br>"+
        "Parentheses and the AND operator can be used to combine expressions, <br><i>e.g. <b>(30 32 34-36) and :A</b> to select residues 30, 32, 34, 35, 36 from chain A <br> <br>"+
        "<a href='https://nglviewer.org/ngl/api/manual/selection-language.html' target='_blank'>See the NGL documentation for more information and other possible selectors</a>";
    showMessage(triggerButton, message);
  }

  /**
  * Returns a summary of the selection, to be shown in the information panel.
  * @return Content of the input field.
  */
  getSelectionDetails() {
    return this.selection.value;
  }

}

export class EpletsSelection extends Selection {
  /* Filtering buttons */
  allEplets;
  confirmedEplets;
  highElliproScoreEplets;

  selectedResidues = []; /* list of residues inculded in the selected eplets */

  /**
  * Generates a EpletsSelection object, which lists all eplets for a structure and enables the user to select them in the 3D structure.
  */
  constructor(structure) {
    super(structure);

    this.inputDiv.classList.add("eplets_input_div");

    // Generates buttons to filter the buttons by status and ElliPro score
    [this.allEplets, this.confirmedEplets, this.highElliproScoreEplets] = this.createFilteringButtons();
    this.inputDiv.append(this.allEplets, this.confirmedEplets, this.highElliproScoreEplets);

    // Generates buttons for every eplets
    this.inputDiv.append(this.create_eplets_list());

    // Adds eventlisteners for the filtering buttons
    this.activateFilteringOfEplets();

    // Adds eventListener for the eplet buttons
    this.listenToEpletsButtons();
  }

  /**
  * Generates three buttons to filter the eplets.
  * @return {array} "All eplets", "Confirmed eplets", "Eplets with a high ElliPro score".
  */
  createFilteringButtons() {
    var allEplets = document.createElement('button');
    allEplets.setAttribute("class", "eplets_selection");
    allEplets.innerHTML = "All eplets";
    allEplets.classList.add("active");

    var confirmedEplets = document.createElement('button');
    confirmedEplets.setAttribute("class", "eplets_selection");
    confirmedEplets.innerHTML = "Confirmed eplets";

    var highElliproScoreEplets = document.createElement('button');
    highElliproScoreEplets.setAttribute("class", "eplets_selection");
    highElliproScoreEplets.innerHTML = "Eplets with a high ElliPro score";

    return [allEplets, confirmedEplets, highElliproScoreEplets];
  }

  /**
  * Generates buttons for every eplet for a given antigen.
  * (= two lists of buttons for DQ and DR antigens (as they have two significant chains)).
  * @return {HTMLElement} Div containing all eplet buttons.
  */
  create_eplets_list() {
    var div = document.createElement('div');
    if (this.antigen.group === "DQ" || this.antigen.group === "DP") {   // two chains
        div.innerHTML += "<br/> Chain A <br/>";
        div.append(this.create_eplets_buttons("A"));
        div.innerHTML += "<br/>Chain B <br/>";
        div.append(this.create_eplets_buttons("B"));
    } else {  // one chain only
      div.append(this.create_eplets_buttons("A"));
      }
    return div;
  }

  /**
  * Generates buttons for every eplet for a given allele.
  * Each button gives the name of the eplet on top, the status of the eplet on the bottom left,
  * the ElliPro score of the eplet on the bottom left, and the list of residues on hover.
  * @return {HTMLElement} Div containing all eplet buttons.
  */
  create_eplets_buttons(chain) {
    var div = document.createElement('div');

    var epletsList;

    if (chain == "A") {
      epletsList = this.antigen.epletsA;
    } else {
      epletsList = this.antigen.epletsB;
    }

    for(var i=0; i < epletsList.length; i++) {
      var eplet = epletsList[i];
      var class_name = "eplet "+chain+" ";
      if (eplet[1] === "H") {
          class_name += " high";
      }
      if (eplet[2] === "C") {
          class_name += " confirmed";
      }

      var button = document.createElement('button');
      button.setAttribute("class", class_name);
      button.setAttribute("title", eplet[3]); // residues

      var epletName = document.createElement('div');
      epletName.setAttribute("class", "name_eplet");
      epletName.innerHTML = eplet[0]; // eplet name
      var info = document.createElement('div');
      info.setAttribute("class", "info_eplets");
      var score = document.createElement('div');
      score.setAttribute("class", "score_eplet");
      score.innerHTML = eplet[1]; // ElliPro score, "x" if none
      var statut = document.createElement("div");
      statut.setAttribute("class", "statut_eplet");
      statut.innerHTML = eplet[2]; // "C" for confirmed, "x" otherwise

      button.append(epletName);
      info.append(score);
      info.append(statut);
      button.append(info);

      div.append(button);
    }
    return div;
  }

  /**
  * Updates the selection when an eplet is selected or deselected.
  */
  listenToEpletsButtons() {
    // Rather than adding an EventListener to each eplet button, we listen for clicks on the whole list of eplets
    let self = this;
    this.inputDiv.addEventListener('click', function (e) {
      /* Gets the button (the user may click on any div inside the actual button) */
      var button;
	    if (e.target.matches('.eplet')) {
        button = e.target;
      } else if (e.target.parentElement.matches('.eplet')) {
        button = e.target.parentElement;
      } else if (e.target.parentElement.parentElement.matches('.eplet')) {  // eplet buttons have children + grandchildren
        button = e.target.parentElement.parentElement;
      } else {
         return; // If the clicked element isn't an eplet button (or a descendant of an eplet button), bail
      }

      /* Gets the name of the eplet and its list of residues */
      var eplet = button.querySelector('.name_eplet').innerHTML;
      var residues = button.title.split(' '); // List of residues

      /* Specifies the chain */
      var chain = "A";  // We have to specify the position + the chain, even for A, B, C, DR, since there's a second chain
      if (self.antigen.group === "DQ" || self.antigen.group === "DP") {   // Two relevant chains
        if (button.classList.contains("B")) chain = "B";
      }

      /* For each residues: */
      for (var i = 0; i < residues.length; i++) {
        /* Gets residue position */
        var position = residues[i].replace( /^\D+/g, '').match(/^\d+/); // Residues are listed as positionAA
        // Residues are also sometimes are listed in parenthesis, or in rare cases given with weird nomenclature such as "150V1"
        // So: 'replace' to delete non-digits leading characters, regex to select leading digits = the position

        /* Adjusts position if need be (in case of indels) */
        var antigen = self.antigen.name;
        if (["DQ", "DP"].includes(self.antigen.group)) {
          if (chain == "A") {
            antigen = antigen.split("-")[1];
          } else {
            antigen = antigen.split("-")[0];
          }
        }
        if (antigen in indels) {
          // Checks whether there is an indel in the allele and if so, updates relevant positions (as the residues are given according to a reference allele)
          if (position > indels[antigen][1]) {
            position += indels[antigen][0];
          }
        } else if (["DQA1*02", "DQA1*04", "DQA1*05", "DQA1*06"].includes(antigen.substring(0,7)) && position > 56) {
          // Checks whether there is a specific deletion shared by multiple DQA1 alleles
          position -= 1;
        }

        /* Updates the list of selected residues (to later update the 3D view) */
        if (isButtonActive(button)) {
          // If the eplet was selected: removes corresponding residues from selection
          self.selectedResidues = removeItemFromArrayOnce(self.selectedResidues, position+":"+chain);
        } else {
          // Else: adds corresponding reisudes to selection
          self.selectedResidues.push(position+":"+chain);
        }
      }

      /* Selects or unselects eplet button */
      if (isButtonActive(button)) {
        button.classList.remove("active");
      } else {
        button.classList.add("active");
      }

      /* From the updated list of selected residues, generates the selection query (NGL syntax) and calls for 3D view to be updated */
      self.previousSelection = self.currentSelection;
      self.currentSelection = "";
      for (var i = 0; i < self.selectedResidues.length; i++) {
        self.currentSelection += self.selectedResidues[i]+" "
      }
      self.updateSelection();

    }, false);
  }

  /**
  * Enables eplets to be filtered by status (confirmed or not) and ElliPro score (high or not).
  * "Confirmed eplets" and "Eplets with a high ElliPro score" can both be selected at the same time. If all buttons are deselected, "All eplets" gets selected.
  */
  activateFilteringOfEplets() {
    // Selection of the eplets buttons
    var all = this.div.getElementsByClassName("eplet");
    var confirmed = this.div.getElementsByClassName("confirmed");
    var high = this.div.getElementsByClassName("high");
    var confirmedAndHigh = this.div.getElementsByClassName("confirmed high");

    let self = this;

    this.allEplets.addEventListener("click", function() {
      if (!isButtonActive(this)) {  // If "all eplets" isn't already selected
        this.classList.add("active");
        // "Confirmed eplets" and "Eplets with a high ElliPro score" deselected
        self.confirmedEplets.className = "eplets_selection";
        self.highElliproScoreEplets.className = "eplets_selection";
        // Display all eplets buttons
        for (var i = 0; i < all.length; i++) {
          all[i].style.display = "inline";
        }
      }
    });

    this.confirmedEplets.addEventListener("click", function() {
      if (isButtonActive(this)) {  // If "confirmed eplets" is already selected
        this.classList.remove("active");
        if (isButtonActive(self.highElliproScoreEplets)) { // If "Eplets with a high ElliPro score" is selected
          for (var i = 0; i < confirmed.length; i++) {
            confirmed[i].style.display = "none";  // Hide the confirmed eplets
          }
          for (var i = 0; i < high.length; i++) {
            high[i].style.display = "inline";  // Display eplets with a high ElliPro score
          }
        } else {  // "Eplets with a high ElliPro score" isn't selected, we revert to "All eplets"
          self.allEplets.classList.add("active");
          for (var i = 0; i < all.length; i++) {
            all[i].style.display = "inline";           // Display all eplets buttons
          }
        }
      } else { // If "confirmed eplets" isn't already selected
        this.classList.add("active");
        if (isButtonActive(self.allEplets)) { // If "All eplets" was selected
          self.allEplets.classList.remove("active");
          for (var i = 0; i < all.length; i++) {
            all[i].style.display = "none";  // Hide all eplets
          }
          for (var i = 0; i < confirmed.length; i++) {
            confirmed[i].style.display = "inline";  // Display eplets that are confirmed
          }
        } else if (isButtonActive(self.highElliproScoreEplets)) { // If "Eplets with a high ElliPro score" was selected
          for (var i = 0; i < high.length; i++) {
            high[i].style.display = "none";  // Hide eplets with a high score
          }
          for (var i = 0; i < confirmedAndHigh.length; i++) {
            confirmedAndHigh[i].style.display = "inline";  // Display the eplets that are both confirmed and high
          }
        }
      }
    });

    this.highElliproScoreEplets.addEventListener("click", function() {
      if (isButtonActive(this)) {  // If "Eplets with a high ElliPro score" is already selected
        this.classList.remove("active");
        if (isButtonActive(self.confirmedEplets)) { // If "Confirmed eplets" is selected
          for (var i = 0; i < high.length; i++) {
              high[i].style.display = "none";  // Hide the eplets with a high score
          }
          for (var i = 0; i < confirmed.length; i++) {
            confirmed[i].style.display = "inline";  // Display confirmed eplets
          }
        } else {  // "Confirmed eplets" isn't selected, we revert to "All eplets"
          self.allEplets.classList.add("active");
          for (var i = 0; i < all.length; i++) {
            all[i].style.display = "inline"; // Display all eplets buttons
          }
        }
      } else { // If "Eplets with a high ElliPro score" isn't already selected
        this.classList.add("active");
        if (isButtonActive(self.allEplets)) { // If "All eplets" was selected
          self.allEplets.classList.remove("active");
          for (var i = 0; i < all.length; i++) {
            all[i].style.display = "none";  // Hide all eplets
          }
          for (var i = 0; i < high.length; i++) {
              high[i].style.display = "inline";  // Display eplets with a high score
          }
        } else if (isButtonActive(self.highElliproScoreEplets)) { // If "Eplets confirmed" was selected
          for (var i = 0; i < confirmed.length; i++) {
            confirmed[i].style.display = "none";  // Hide confirmed eplets
          }
          for (var i = 0; i < confirmedAndHigh.length; i++) {
            confirmedAndHigh[i].style.display = "inline";  // Display the eplets that are both confirmed and high
          }
        }
      }
    });
  }

  /**
  * Returns a summary of the selection, to be shown in the information panel.
  * @return List of selected eplets, with chains if relevant.
  */
  getSelectionDetails() {
    var details = "";
    var eplets = this.inputDiv.getElementsByClassName("eplet");
    var count = 0;

    if (this.antigen.group == "DQ" || this.antigen.group == "DP") { // two chains
      var chainA = "";
      var chainB = "";
      for (let i=0; i < eplets.length; i++) {
        if (eplets[i].classList.contains("active")) {
          count++;
          if (eplets[i].classList.contains("A")) {
            chainA += eplets[i].querySelector(".name_eplet").innerHTML+", ";
          } else {
            chainB += eplets[i].querySelector(".name_eplet").innerHTML+", ";
          }
        }
      }
      if (chainA != "") {
        details += chainA.slice(0,-2)+" (chain A)"; // slice to delete the last comma
      }
      if (chainA != "" && chainB != "") {
        details += " and ";
      }
      if (chainB != "") {
        details += chainB.slice(0,-2)+" (chain B)";
      }
    } else {
      for (let i=0; i < eplets.length; i++) {
        if (eplets[i].classList.contains("active")) {
            count++;
            details += eplets[i].querySelector(".name_eplet").innerHTML+", ";
        }
      }
      details = details.slice(0,-2);
    }
    if (count > 1) {
      details = "Eplets "+details;
    } else {
      details = "Eplet "+details;
    }
    return details;
  }

}


export class PatchSelection extends Selection {
  detailsDiv; /* to list the residues included in a patch */

  /**
  * Generates a PatchSelection object, which enables the user to select a 'patch' of a given radius around a given residue.
  */
  constructor(structure) {
    super(structure);

    this.inputDiv.classList.add("patches_input_div");

    // Generates input fields for the central residue and the radius
    var centralResidue = document.createElement('input');
    centralResidue.setAttribute("class", "patch_central_residue_input");
    centralResidue.setAttribute("type", "text");
    centralResidue.setAttribute("placeholder", "Central residue");
    this.inputDiv.append(centralResidue);

    this.inputDiv.insertAdjacentHTML("beforeend", " - ");

    var radius = document.createElement('input');
    radius.setAttribute("class", "patch_radius_input");
    radius.setAttribute("type", "text");
    radius.setAttribute("placeholder", "Radius");
    this.inputDiv.append(radius);

    var unit = document.createElement('span');
    unit.setAttribute("class", "patch_radius_unit");
    unit.innerHTML = "Å"; // Angstrom
    this.inputDiv.append(unit);

    // Generates a submit button
    var button = document.createElement('button');
    button.setAttribute("class", "validate_selection");
    button.innerHTML = "OK";
    this.inputDiv.append(button);

    // Generates a div to list the residues included in a patch
    this.detailsDiv = document.createElement('div');
    this.detailsDiv.setAttribute("class", "patch_details");
    this.inputDiv.append(this.detailsDiv);

    // Adds eventListener
    let self = this;
    button.addEventListener("click", function() {
      self.onValidation();
    });
  }

  /**
  * Updates the selection.
  */
  onValidation() {
    var centralResidue = this.inputDiv.querySelector('.patch_central_residue_input').value;
    var radius = this.inputDiv.querySelector('.patch_radius_input').value;

    // First we select a patch of a given radius around the central residue:
    var atomSet = this.parent.NGLstructure.structure.getAtomSetWithinSelection( new NGL.Selection( centralResidue ), radius );
    // Then we expand that selection to complete groups (residues):
    var atomSet2 = this.parent.NGLstructure.structure.getAtomSetWithinGroup( atomSet );

    this.previousSelection = this.currentSelection;
    this.currentSelection = atomSet2.toSeleString(); // translation to the NGL selection syntax

    // Updates the selection
    this.updateSelection();
    this.displayDetails();
  }

  /**
  * Lists residues included in a patch.
  */
  displayDetails() {
    var aa = {'CYS': 'C', 'ASP': 'D', 'SER': 'S', 'GLN': 'Q', 'LYS': 'K',
     'ILE': 'I', 'PRO': 'P', 'THR': 'T', 'PHE': 'F', 'ASN': 'N',
     'GLY': 'G', 'HIS': 'H', 'HSD': 'H', 'LEU': 'L', 'ARG': 'R', 'TRP': 'W',
     'ALA': 'A', 'VAL':'V', 'GLU': 'E', 'TYR': 'Y', 'MET': 'M'};
    var details = "Residues: ";
    var chainA = "";
    var chainB = "";
    this.parent.NGLstructure.structure.getView( new NGL.Selection( this.currentSelection ) ).eachResidue(function(r) {
        if (r.chain.chainname == "B") {
          chainB += r.resno+aa[r.resname]+", ";
        } else {
          chainA += r.resno+aa[r.resname]+", ";
        }
    });
    if (chainA != "") {
      details += chainA.slice(0,-2)+" (chain A)"; // slice to delete the last comma
    }
    if (chainA != "" && chainB != "") {
      details += " and ";
    }
    if (chainB != "") {
      details += chainB.slice(0,-2)+" (chain B)";
    }
    this.detailsDiv.innerHTML = details;
  }

  /**
  * Returns a summary of the selection, to be shown in the information panel.
  * @return Description of the patch.
  */
  getSelectionDetails() {
    return this.inputDiv.querySelector('.patch_radius_input').value+"-Å patch around the residue "+this.inputDiv.querySelector('.patch_central_residue_input').value;
  }
}
