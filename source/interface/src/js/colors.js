/**
* @file Miscallenous constants and functions relating to colors.
* Most of these functions copied from StackOverflow or other online sources.
*/

/* Colors used for 3D structures and selections */
export const defaultColors = ['#377eb8', '#e31a1c', '#ff7f00', '#33a02c', '#984ea3', '#fed64a', '#f0b7b3', '#183b8b'];
// blue, red, orange, green, purple, yellow, light pink, darker blue

/* Gradients available to the user for color schemes (e.g. hydrophobicity)
Key: description of the gradient shown to the user
Value: name of the gradient as understood by NGL (either a defined name or an array)
*/
export const gradients = {
  "Orange-Red" : "OrRd",
  "Purple-Blue" : "PuBu",
  "Red-Yellow-Green" : "RdYlGn",
  "Red-White-Blue" : "rwb"
};
// See http://nglviewer.org/ngl/api/file/src/color/colormaker-registry.js.html for the complete list of gradients defined in NGL
// Other gradients have to be given as an array of colors

/* Gradients available to the user for color schemes
Key: name of the gradient as understood by NGL
Value: comma-separated colors in gradient */
const colorGradientsNamesToList = { "OrRd": "Orange, Red",
        "PuBu": "Purple, Blue",
        "RdYlGn": "Red, Yellow, Green",
        "rwb": "Red, White, Blue" };

/**
* From the color parameters of a 3D element, returns a visual summary to be displayed in the information panel.
* @param {object} colorParameters NGL parameters.
* @return {HTMLElement} Span. Either a color dot or the color scheme name surrounded by the corresponding gradient.
*/
export function getColorSummary(colorParameters) {
  if (colorParameters.colorScheme == "uniform") {
    var colorDot = document.createElement('span');
    colorDot.setAttribute("class", "info_panel_color_dot");
    colorDot.style.backgroundColor = colorParameters.colorValue;
    return colorDot;
  }
  else {
    var colorScheme = document.createElement('span');
    colorScheme.setAttribute("class", "info_panel_color_scheme_name_wrap");

    var colorSchemeName = document.createElement('div');
    colorSchemeName.setAttribute("class", "info_panel_color_scheme_name");
    colorScheme.append(colorSchemeName);

    if (colorParameters.colorScheme == "chainname") {
      colorSchemeName.innerHTML = "by chain";
      colorScheme.style.background = "linear-gradient(100deg, "+colorParameters.colorScale[1]+" 0 50%, "+colorParameters.colorScale[0]+" 50% 100%)";
      // To be coherent with the name of the antigens (e.g. "DQB1*02:01-DQA1*02:01"), we first give the color of the beta chain (second argument) and then of the alpha chain (first argument)
    } else {
      var colors;
      if (colorParameters.colorScheme.includes("rsasa")) {
        // As it's a custom color scheme, its name is [random name]|[name]
        // in our case, the name is set to "rsasa|[gradient]"
        colorSchemeName.innerHTML = "RSASA";
        colors = getColorsOfGradient(colorParameters.colorScheme.split("|")[2]);
      } else {
        colorSchemeName.innerHTML = colorParameters.colorScheme;
        colors = getColorsOfGradient(colorParameters.colorScale);
      }
      colorScheme.style.background = "linear-gradient(100deg,"+colors+")";
    }
    return colorScheme;
  }
}

/**
* Lists the defining colors of a gradient.
* @param {str or array} gradient Name of the gradient as understood by NGL (defined name like "OrRd" or array of colors).
* (For RSASA: said name but stringified (everything in lowercase and array will be a comma-separated list).)
* @return {str} Comma-separated colors
*/
export function getColorsOfGradient(gradient) {
  if (!Array.isArray(gradient)) {  // defined gradient name like "OrRd"
    return colorGradientsNamesToList[Object.keys(colorGradientsNamesToList).find(key => key.toLowerCase() === gradient.toLowerCase())]; // case insensitive search
  } else if (!gradient.includes(',')) {
    var colors;
    for (let i=0; i < gradient.length; i++) {
      colors += gradient[i]+",";
    }
    return colors.slice(0,-1);
  } else {  // already a comma-separated list
    return gradient;
  }
}

/**
* Given a color, returns a lighter or darker shade.
* Function by Chris Coyier, css-tricks.com/snippets/javascript/lighten-darken-color/
* @param {str} color Color (hex).
* @param {int} amount Positive to lighten, negative to darken.
* @return {str} Color (hex).
*/
export function lightenDarkenColor(color, amount) {
  color = color.slice(1); // remove #
  var num = parseInt(color, 16);
  var r = (num >> 16) + amount;
  if (r > 255) r = 255;
  else if (r < 0) r = 0;
  var b = ((num >> 8) & 0x00FF) + amount;
  if (b > 255) b = 255;
  else if (b < 0) b = 0;
  var g = (num & 0x0000FF) + amount;
  if (g > 255) g = 255;
  else if (g < 0) g = 0;
  return "#" + (g | (b << 8) | (r << 16)).toString(16);
}

/**
* Interpolates a list of colors between two given colors.
* Based on https://graphicdesign.stackexchange.com/questions/83866/generating-a-series-of-colors-between-two-colors/83869
* @param {str} color1 First color (hexadecimal format).
* @param {str} color2 Second color (hexadecimal format).
* @param {int} steps Desired number of in-between colors.
* @return {array} Array of interpolated colors (hexadecimal format).
*/
function interpolateColors(color1, color2, steps) {
  var stepFactor = 1 / (steps - 1),
        interpolatedColorArray = [];
  for(var i = 0; i < steps; i++) {
      interpolatedColorArray.push(interpolateColor(color1, color2, stepFactor * i));
  }
  return interpolatedColorArray;
}

/**
* Returns a single color interpolation between two colors.
* Based on https://codepen.io/njmcode/pen/axoyD?editors=0010
* @param {str} color1 First color (hexadecimal format).
* @param {str} color2 Second color (hexadecimal format).
* @param {float} factor Defines where in-between the colors to pick. By default: 0.5 ("middle" color).
* @return {str} Interpolated color (hexadecimal format).
*/
function interpolateColor(color1, color2, factor = 0.5) {
    color1 = hexToRgb(color1);
    color2 = hexToRgb(color2);
    var result = color1.slice();
    for (var i = 0; i < 3; i++) {
        result[i] = Math.round(result[i] + factor * (color2[i] - color1[i]));
    }
    return rgbToHex(result);
};

/**
* Given a gradient, lists 20 color points.
* @param {str or array} gradient Name of the gradient as understood by NGL (defined name like "OrRd" or array of colors).
* @return {array} Interpolated colors (hexadecimal format).
*/
export function getTwentyColorPoints(gradient) {
  var colorPoints = [];
  var colors = getColorsOfGradient(gradient).replaceAll(' ', '').split(",");
  for (var i = 0; i < colors.length; i++) {
    if (colors[i].substring(0,1) != "#") colors[i] = colourNameToHex(colors[i]);
  }
  var steps = Math.round( 20 / (colors.length-1));
  for (var i = 0; i < colors.length - 2; i++) {
    colorPoints = colorPoints.concat(interpolateColors(colors[i], colors[i+1], steps));
  }
  if (steps*(colors.length-1) != 20) steps++;
  colorPoints = colorPoints.concat(interpolateColors(colors[colors.length-2], colors[colors.length-1], steps));
  return colorPoints;
}

/**
* Converts a #ffffff hex string into an [r,g,b] array.
*/
function hexToRgb(hex) {
  var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return [parseInt(result[1], 16), parseInt(result[2], 16), parseInt(result[3], 16)];
}

/**
* Converts an [r,g,b] array into a #ffffff hex string.
*/
function rgbToHex(rgb) {
    return "#" + ((1 << 24) + (rgb[0] << 16) + (rgb[1] << 8) + rgb[2]).toString(16).slice(1);
};

/**
* Converts a defined color name into the hexadecimal format.
*/
function colourNameToHex(colour)
{
    var colours = {"aliceblue":"#f0f8ff","antiquewhite":"#faebd7","aqua":"#00ffff","aquamarine":"#7fffd4","azure":"#f0ffff",
    "beige":"#f5f5dc","bisque":"#ffe4c4","black":"#000000","blanchedalmond":"#ffebcd","blue":"#0000ff","blueviolet":"#8a2be2","brown":"#a52a2a","burlywood":"#deb887",
    "cadetblue":"#5f9ea0","chartreuse":"#7fff00","chocolate":"#d2691e","coral":"#ff7f50","cornflowerblue":"#6495ed","cornsilk":"#fff8dc","crimson":"#dc143c","cyan":"#00ffff",
    "darkblue":"#00008b","darkcyan":"#008b8b","darkgoldenrod":"#b8860b","darkgray":"#a9a9a9","darkgreen":"#006400","darkkhaki":"#bdb76b","darkmagenta":"#8b008b","darkolivegreen":"#556b2f",
    "darkorange":"#ff8c00","darkorchid":"#9932cc","darkred":"#8b0000","darksalmon":"#e9967a","darkseagreen":"#8fbc8f","darkslateblue":"#483d8b","darkslategray":"#2f4f4f","darkturquoise":"#00ced1",
    "darkviolet":"#9400d3","deeppink":"#ff1493","deepskyblue":"#00bfff","dimgray":"#696969","dodgerblue":"#1e90ff",
    "firebrick":"#b22222","floralwhite":"#fffaf0","forestgreen":"#228b22","fuchsia":"#ff00ff",
    "gainsboro":"#dcdcdc","ghostwhite":"#f8f8ff","gold":"#ffd700","goldenrod":"#daa520","gray":"#808080","green":"#008000","greenyellow":"#adff2f",
    "honeydew":"#f0fff0","hotpink":"#ff69b4",
    "indianred ":"#cd5c5c","indigo":"#4b0082","ivory":"#fffff0","khaki":"#f0e68c",
    "lavender":"#e6e6fa","lavenderblush":"#fff0f5","lawngreen":"#7cfc00","lemonchiffon":"#fffacd","lightblue":"#add8e6","lightcoral":"#f08080","lightcyan":"#e0ffff","lightgoldenrodyellow":"#fafad2",
    "lightgrey":"#d3d3d3","lightgreen":"#90ee90","lightpink":"#ffb6c1","lightsalmon":"#ffa07a","lightseagreen":"#20b2aa","lightskyblue":"#87cefa","lightslategray":"#778899","lightsteelblue":"#b0c4de",
    "lightyellow":"#ffffe0","lime":"#00ff00","limegreen":"#32cd32","linen":"#faf0e6",
    "magenta":"#ff00ff","maroon":"#800000","mediumaquamarine":"#66cdaa","mediumblue":"#0000cd","mediumorchid":"#ba55d3","mediumpurple":"#9370d8","mediumseagreen":"#3cb371","mediumslateblue":"#7b68ee",
    "mediumspringgreen":"#00fa9a","mediumturquoise":"#48d1cc","mediumvioletred":"#c71585","midnightblue":"#191970","mintcream":"#f5fffa","mistyrose":"#ffe4e1","moccasin":"#ffe4b5",
    "navajowhite":"#ffdead","navy":"#000080",
    "oldlace":"#fdf5e6","olive":"#808000","olivedrab":"#6b8e23","orange":"#ffa500","orangered":"#ff4500","orchid":"#da70d6",
    "palegoldenrod":"#eee8aa","palegreen":"#98fb98","paleturquoise":"#afeeee","palevioletred":"#d87093","papayawhip":"#ffefd5","peachpuff":"#ffdab9","peru":"#cd853f","pink":"#ffc0cb","plum":"#dda0dd","powderblue":"#b0e0e6","purple":"#800080",
    "rebeccapurple":"#663399","red":"#ff0000","rosybrown":"#bc8f8f","royalblue":"#4169e1",
    "saddlebrown":"#8b4513","salmon":"#fa8072","sandybrown":"#f4a460","seagreen":"#2e8b57","seashell":"#fff5ee","sienna":"#a0522d","silver":"#c0c0c0","skyblue":"#87ceeb","slateblue":"#6a5acd","slategray":"#708090","snow":"#fffafa","springgreen":"#00ff7f","steelblue":"#4682b4",
    "tan":"#d2b48c","teal":"#008080","thistle":"#d8bfd8","tomato":"#ff6347","turquoise":"#40e0d0",
    "violet":"#ee82ee",
    "wheat":"#f5deb3","white":"#ffffff","whitesmoke":"#f5f5f5",
    "yellow":"#ffff00","yellowgreen":"#9acd32"};

    return colours[colour.toLowerCase()];
}
