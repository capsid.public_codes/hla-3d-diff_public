/**
* @file Manages StyleWindow objects.
* A StyleWindow object manages the style of a 3D element: either its representations (e.g. 'cartoon', 'licorice') or its color parameters (e.g. 'uniform' + 'blue', 'hydrophobicity' + 'Red-Yellow-Green').
* It corresponds to a window in the interface menu.
* @module StyleWindow
* @module RepresentationWindow
* @module ColorWindow
* @module ColorButton
* @module ColorButtonByChain
* @module ColorButtonWithGradient
*/

import {isButtonActive, toggle_button, showMessage} from './misc.js';
import {defaultColors, gradients, lightenDarkenColor, getColorsOfGradient, getTwentyColorPoints} from './colors.js';

export class StyleWindow {
  parent; /* Structure or Selection object */
  structure; /* "main" or "superposed" */

  /* A StyleWindow object corresponds to a popup window in the interface menu.
  It is linked to a parameter button in the parent Structure / Selection object.
  On click, the window is made visible, with a small arrow pointing to the button. */
  div; /* window */
  arrow; /* extension of the window, pointing on the button that opens the window */
  correspondingButton;

  /**
  * Generates a StyleWindow object, which enables the user to manage the style of the parent 3D element.
  * @param {Structure or Selection} parent Parent Structure or Selection object.
  * @param {HTMLElement} correspondingButton Button to open the style window.
  * @param {str} structure "main" or "superposed".
  */
  constructor(parent, correspondingButton, structure) {
    this.parent = parent;
    this.correspondingButton = correspondingButton;
    this.structure = structure;

    // Generates the basic window
    this.div = document.createElement('div');
    this.div.setAttribute("class", "style_window");
    this.div.style.display = "none";

    this.arrow = document.createElement('div');
    this.arrow.setAttribute("class", "style_window_arrow");
    this.arrow.style.display = "none";

    document.body.append(this.div);
    document.body.append(this.arrow);
  }

  /**
  * Creates a button to be added to the window.
  * The button is made up of an image and a label.
  * @param {str} label Label, e.g. 'Cartoon'.
  * @param {str} imageName Name of the image. File should be a .png in the /image/ folder.
  * @return {HTMLElement} Button.
  */
  addButton(label, imageName) {
    var button = document.createElement('button');
    button.setAttribute("class", "style_button");

    var img = document.createElement('IMG');
    img.setAttribute("src", "./images/"+imageName+".png");
    img.style.cssText = "width:80px;height:80px;";

    button.append(img);
    button.innerHTML += "</br>"+label;

    return button;
  }

  /**
  * Hides / shows window.
  */
  toggleWindowVisibility() {
    if (this.div.style.display == "none") {
      this.show();
    } else {
      this.hide();
    }
  }

  /**
  * Hides window.
  */
  hide() {
    this.div.style.display = "none";
    this.arrow.style.display = "none";
  }

  /**
  * Shows window and moves it to be right next to the button that opens it.
  */
  show() {
    this.div.style.display = "block";
    this.arrow.style.display = "block";

    // Adjust window position to button position (as button can move in menu)

    var documentBoudingClientRect = document.body.getBoundingClientRect();
    var buttonBoundingClientRect = this.correspondingButton.getBoundingClientRect();
    var buttonPaddingLeft = parseInt(window.getComputedStyle(this.correspondingButton).paddingLeft);
    var buttonPaddingTop = parseInt(window.getComputedStyle(this.correspondingButton).paddingTop);

    // Note: in css, 'right' measures the distance from the right edge of the window to the right edge of the element
    // while the measure 'right' returned by getBoundingClientRect() is the distance between the left edge of the window and the right edge of the element

    this.arrow.style.right = documentBoudingClientRect.right - buttonBoundingClientRect.left - buttonPaddingLeft + 2 + "px"; // so that the window begins at the left edge of the corresponding button
    this.arrow.style.top = buttonBoundingClientRect.top + buttonPaddingTop - 10 + "px";
    this.div.style.right = documentBoudingClientRect.right - buttonBoundingClientRect.left - buttonPaddingLeft + 15 + "px";
    this.div.style.top = Math.min(buttonBoundingClientRect.top + buttonPaddingTop - 15, documentBoudingClientRect.bottom - 150) + "px"; // to avoid overflowing at the bottom
  }

  /**
  * Defines the eventlistener that will close the window if the user clicks anywhere else than the open window / the button.
  * Note: This function has to be called after all elements of the window (the different buttons, etc.) are generated.
  */
  closeIfOutsideClick() {
    let self = this;
    var descendants = this.div.querySelectorAll("*");
    document.addEventListener("click", function(e) {
      if (self.div.style.display == "block") {
        var close = true;
        if (e.target === self.correspondingButton || e.target === self.div) {
          close = false;
        } else {
          for (let i = 0; i < descendants.length; i++) {
            if (e.target === descendants[i]) {
              close = false;
              break;
            }
          }
        }
        if (close) {
          self.hide();
        }
      }
    });
  }

}

export class RepresentationWindow extends StyleWindow {
  /* Molecular representations available in NGL are listed here: https://nglviewer.org/ngl/api/manual/molecular-representations.html */

  /* We picked the following representations: cartoon, licorice, ball & stick, spacefill and surface.
  Note that 'cartoon' only works for selections of 4 or more adjacents residues, and as such, is only used for whole structures. */

  /* To add a representation, simply add to the following 2 lists and add corresponding picture in folder ./images/. */

  /* Names of the representations, to be displayed in the buttons: */
  static representations_name = ['Cartoon', 'Licorice', 'Ball & stick', 'Spacefill', 'Surface'];
  /* Names of the representations in the NGL syntax, also used to name the pictures: */
  static representations_abbreviations = ['cartoon', 'licorice', 'ball+stick', 'spacefill', 'surface'];

  /**
  * Generates a RepresentationWindow object, which enables the user to add and delete representations for the parent 3D element.
  * Buttons are independant, several representations can be selected at once.
  */
  constructor(parent, correspondingButton, structure) {
    super(parent, correspondingButton, structure);

    // Adds buttons to the window
    this.generateButtons();

    // Adds eventlistener so that if the user clicks anywhere else than the open window, the window closes
    this.closeIfOutsideClick();
  }

  /**
  * Adds one button for each representation to the window.
  * A button is made up of a picture of the representation and the name of the representation.
  * Below each button is a slider to set the opacity of the representation (only shown when the button is selected).
  */
  generateButtons() {
    var defaultRepr = true;
    for (let i=0; i < RepresentationWindow.representations_name.length; i++) {
      // For selections, we'll ignore the first representation, "cartoon", as NGL doesn't display selections of less than 4 residues as cartoon.
      if (!(this.parent.constructor.name.includes("Selection") && RepresentationWindow.representations_name[i] == "Cartoon")) {
        var span = document.createElement('span');
        span.style.cssText = "display: inline-block; width: 95px; vertical-align: top;";

        // Creates the button (picture + label)
        var button = this.addButton(RepresentationWindow.representations_name[i], RepresentationWindow.representations_abbreviations[i]); // label, picture name
        span.append(button);

        // Creates the slider to set the opacity of the representation
        var opacityDiv = document.createElement('div');
        opacityDiv.innerHTML = "Opacity: ";
        opacityDiv.style.cssText = "text-align: center; font-size: 12px; display: none;";
        opacityDiv.setAttribute("class", "opacity_slider");
        var opacitySlider = document.createElement('input');
        opacitySlider.setAttribute("type", "range");
        opacitySlider.setAttribute("min", 0);
        opacitySlider.setAttribute("max", 1);
        opacitySlider.setAttribute("value", 1);
        opacitySlider.setAttribute("step", 0.01);
        opacitySlider.style.cssText = "width: 80px;";
        opacityDiv.append(opacitySlider);
        span.append(opacityDiv);

        this.div.append(span);

        // Adds eventlisteners to button and opacity slider.

        let self = this;
        button.addEventListener("click", function(){self.onButtonClick(this, RepresentationWindow.representations_abbreviations[i])});
        opacitySlider.addEventListener("change", function(){self.onOpacityChange(this, RepresentationWindow.representations_abbreviations[i])});

        // Selects the first button by default
        // It will be 'Cartoon' for a Structure object, and 'Licorice' for a Selection object.
        if (defaultRepr) {
          this.onButtonClick(button, RepresentationWindow.representations_abbreviations[i]);
          defaultRepr = false;
        }
      }
    }
  }

  /**
  * Called when the user selects or deselects a representation.
  * Adjusts style of button and sends information to parent (Structure or Selection object) to add / delete 3D representation.
  * @param {HTMLElement} button Representation button on which the user clicked.
  * @param {str} reprName Name of the representation (NGL syntax).
  */
  onButtonClick(button, reprName) {
    var opacitySliderDiv = button.parentElement.querySelector(".opacity_slider");
    if (isButtonActive(button)) {
      // Desactivates button
      button.classList.remove("active");
      // Hides opacity slider
      opacitySliderDiv.style.display = "none";
      // Deletes representation
      this.parent.deleteRepr(reprName);
    } else {
      // Activates button
      button.classList.add("active");
      // Shows opacity slider
      opacitySliderDiv.style.display = "block";
      // Adds representation
      this.parent.addRepr(reprName, opacitySliderDiv.querySelector("input").value);
    }
  }

  /**
  * Called when the user modifies the value of an opacity slider.
  * Sends information to parent (Structure or Selection object) to adjust opacity of 3D representation.
  * @param {HTMLElement} slider Opacity slider.
  * @param {str} reprName Name of the representation (NGL syntax).
  */
  onOpacityChange(slider, reprName) {
    this.parent.changeReprOpacity(reprName, slider.value);
  }

  /**
  * For a given representation button, returns the value of the associated opacity slider.
  * @param {HTMLElement} button Representation button.
  * @return {int} Value of the opacity slider (between 0 and 100).
  */
  getOpacity(button) {
    return button.parentElement.querySelector(".opacity_slider").value;
  }

}

export class ColorWindow extends StyleWindow {
  /* Color schemes in NGL are listed here: https://nglviewer.org/ngl/api/manual/coloring.html
  * It's also possible to add custom color schemes. */

  /* We picked the following color schemes: uniform (default), by chain (default), hydrophobicity (default) and RSASA (custom, when data available). */

  /* To add a color scheme, modify the function generateButtons(), the necessary functions in the ColorButton class, and add the corresponding picture in folder ./images/.
  For a custom color scheme, the function getColorSummary() in colors.js will also need to be updated. */

  colorButtons = []; /* list of ColorButton objects */
  currentColorParameters; /* current color parameters in NGL format, needed to summarize the color parameters of an element in the information panel */

  /* By default, the color scheme 'uniform' is selected, with an arbitrary color that the user can later modify.
  * Default colors are picked amongst a list of contrasting colors (colors.js).
  * Each new ColorWindow is assigned an id in order to cycle through that list. */
  static nextId = 0;

  /**
  * Generates a ColorWindow object, which enables the user to change the color parameters of the parent 3D element.
  * Buttons are not independant, only one color scheme can be selected at once.
  */
  constructor(parent, correspondingButton, structure) {

    super(parent, correspondingButton, structure);

    this.id = ColorWindow.nextId; // To pick a different default color with each new Structure / Selection object
    ColorWindow.nextId++;
    if (ColorWindow.nextId > defaultColors.length) { ColorWindow.nextId = 0 }

    // Adds buttons to the window
    this.generateButtons();

    // Adds eventlistener so that if the user clicks anywhere else than the open window, the window closes
    this.closeIfOutsideClick();
  }

  /**
  * Adds one button for each color scheme to the window.
  * A button is made up of a picture of the color scheme and the name of the color scheme.
  * Below the buttons are options to pick colors or a gradient (depends on the color scheme).
  * Each button is linked to one div with such options, and when a button is selected its associated div will be made visible.
  */
  generateButtons() {
    var colorPickerDiv = document.createElement('div');

    let self = this;

    // Creates the button (picture + label) and the controls to change the color parameters
    // (The eventlisteners are generated in the ColorButton objects)

    // One by one, as they all have their specificities:
    var uniformColorButton = new ColorButton(self, "Uniform", "Uniform");
    this.colorButtons.push(uniformColorButton);
    uniformColorButton.onColorButtonClick();  // Sets as default

    this.colorButtons.push(new ColorButtonByChain(self, "By chain", "Bychain"));

    this.colorButtons.push(new ColorButtonWithGradient(self, "Hydrophobicity", "Hydrophobicity"));

    if (this.parent.antigen.sasa != null) { // If RSASA data is available
      if (this.parent.constructor.name == "Structure") { // For now, only for the whole structure
        this.colorButtons.push(new ColorButtonWithGradient(self, "RSASA", "RSASA"));
      }
    }

    // Appends buttons and colorPickerDivs to the color window
    for (let i=0; i < this.colorButtons.length; i++) {
      this.div.append(this.colorButtons[i].button);
      colorPickerDiv.append(this.colorButtons[i].colorPickerDiv);
    }
    this.div.append(colorPickerDiv);
  }

  /**
  * Called by a ColorButton object when a button is selected: cycles through all buttons to find the previously selected one and deselects it.
  */
  desactivateButtons() {
    for (let i=0; i < this.colorButtons.length; i++) {
      this.colorButtons[i].desactivate();
    }
  }

  /**
  * Called by a ColorButton object when a button is selected, sends information to parent (Structure / Selection object) to modify 3D element.
  * Also updates the value of currentColorParameters.
  * @param {array} colorParameters New color parameters in NGL format.
  */
  changeColor(colorParameters) {
    this.parent.changeColor(colorParameters);
    this.currentColorParameters = colorParameters;
  }
}

class ColorButton {
  parent; /* parent ColorWindow object */
  colorSchemeLabel; /* name of the color scheme, as will be displayed */
  colorSchemeName; /* name of the color scheme in the NGL syntax */

  button; /* HTML button, contains an image and a title */
  colorPickerDiv; /* HTML div, contains the controls for the parameters of the color scheme */
  /* It can be a color picker (color scheme: 'uniform'), two color pickers ('by chain'), or a list of gradients ('hydrophobicity', 'RSASA'). */

  /**
  * Generates a ColorButton object, which enables the user to select a color scheme and manage its parameters.
  * It corresponds to a button that is added to the parent color window.
  * The class ColorButton serves as both a template for ColorButtonByChain and ColorButtonWithGradient and to construct the button for the color scheme 'Uniform'.
  * @param {ColorWindow} parent Parent ColorWindow object.
  * @param {str} colorSchemeLabel Title of the color scheme.
  * @param {str} colorSchemeName Name of the color scheme in the NGL syntax.
  */
  constructor(parent, colorSchemeLabel, colorSchemeName) {
    this.parent = parent;
    this.colorSchemeLabel = colorSchemeLabel;
    this.colorSchemeName = colorSchemeName;

    // Creates button
    this.button = this.parent.addButton(this.colorSchemeLabel, this.colorSchemeName);

    // Adds eventlistener to the button
    let self = this;
    this.button.addEventListener("click", function() {
      self.onColorButtonClick();
    });

    // Creates the div with color parameters (eventlisteners added in the function)
    this.colorPickerDiv = this.addColorPicker();

    // The contents of 'button' and 'colorPickerDiv' will then be called in the parent ColorWindow and added to the color window.
  }

  /**
  * Generates a div with a single color picker for the color scheme 'Uniform'.
  * @return {HTMLElement} The div.
  */
  addColorPicker() {
    var div = document.createElement("div");
    div.style.display = "none";

    var colorPicker = document.createElement('input');
    colorPicker.setAttribute("type", "color");
    colorPicker.setAttribute("value", defaultColors[this.parent.id]);

    let self = this;
    colorPicker.addEventListener("input", function() {
      self.onColorChange();
    });

    div.append(colorPicker);
    return div;
  }

  /**
  * Called when the user selects a color scheme.
  * Calls for the previously selected button to be unselected and sends information about new color parameters to parent to adjust 3D representation.
  */
  onColorButtonClick() {
    // Calls for the previously selected button to be unselected
    this.parent.desactivateButtons();
    // Adjusts button to the 'selected' style
    this.button.classList.add("active");
    // Makes associated colorPickerDiv visible
    this.colorPickerDiv.style.display = "block";

    // Sends information about new color parameters to parent to adjust 3D representation.
    this.onColorChange();
  }

  /**
  * Generates color parameters for the color scheme 'Uniform' and sends information to parent to adjust 3D representation.
  */
  onColorChange() {
    var color = this.colorPickerDiv.querySelector('input').value; // currently selected color
    var colorParameters = {colorScheme: "uniform", colorValue: color};
    this.parent.changeColor(colorParameters);
  }

  /**
  * If button is selected, deselects it and hides corresponding colorPickerDiv (+ scale for color schemes that have one).
  */
  desactivate() {
    if (this.button.classList.contains("active")) {
      this.button.classList.remove("active");
      this.colorPickerDiv.style.display = "none";
      if (this.constructor.name == "ColorButtonWithGradient") {
        // Hide scale
        this.scaleDiv.style.display = "none";
      }
    }
  }
}

class ColorButtonByChain extends ColorButton {

  /**
  * Generates a ColorButton object for the color scheme 'By chain'.
  */
  constructor(parentWindow, colorSchemeLabel, colorSchemeName) {
    super(parentWindow, colorSchemeLabel, colorSchemeName);
  }

  /**
  * Generates a div with two color pickers for the color scheme 'By chain'.
  * @return {HTMLElement} The colorPickerDiv.
  */
  addColorPicker() {
    var div = document.createElement("div");
    div.style.display = "none";

    var colorPickerChainA = document.createElement('input');
    colorPickerChainA.setAttribute("type", "color");
    colorPickerChainA.setAttribute("class", "chainA");

    var colorPickerChainB = document.createElement('input');
    colorPickerChainB.setAttribute("type", "color");
    colorPickerChainB.setAttribute("class", "chainB");

    // Default colors: a lighter and darker shades of the initial default color
    colorPickerChainA.setAttribute("value", lightenDarkenColor(defaultColors[this.parent.id], -15));
    colorPickerChainB.setAttribute("value", lightenDarkenColor(defaultColors[this.parent.id], 45));

    let self = this;
    colorPickerChainA.addEventListener("input", function() {
      self.onColorChange();
    });
    colorPickerChainB.addEventListener("input", function() {
      self.onColorChange();
    });

    div.insertAdjacentHTML("beforeend", "Chain B: ");
    div.append(colorPickerChainB);
    div.insertAdjacentHTML("beforeend", " - Chain A: ");
    div.append(colorPickerChainA);

    return div;
  }

  /**
  * Generates color parameters for the color scheme 'By chain' and sends information to parent to adjust 3D representation.
  */
  onColorChange() {
    var colorA = this.colorPickerDiv.querySelector('.chainA').value;
    var colorB = this.colorPickerDiv.querySelector('.chainB').value;

    var colorParameters = {colorScheme: "chainname", colorScale: [colorA, colorB]};

    this.parent.changeColor(colorParameters);
  }

}

class ColorButtonWithGradient extends ColorButton {

  scaleDiv; /* div in the left corner of the interface, contains a labelled scale showing the selected gradient */

  /**
  * Generates a ColorButton object for color schemes with gradients ('Hydrophobicity', 'RSASA').
  */
  constructor(parentWindow, colorSchemeLabel, colorSchemeName) {
    super(parentWindow, colorSchemeLabel, colorSchemeName);

    // Adds a scale on the left corner of the interface (only visible when the color scheme is selected)
    this.addScale();
  }

  /**
  * Generates a div with a dropdown of available gradients.
  * Uses gradients listed in the constant 'gradients' (colors.js).
  * @return {HTMLElement} The colorPickerDiv.
  */
  addColorPicker() {
    var div = document.createElement("div");
    div.style.display = "none";

    var gradientDropdown = document.createElement('select');
    let self = this;
    gradientDropdown.addEventListener("change", function() {
      self.onColorChange();
    });

    for (var gradient in gradients) {
      var option = document.createElement("option");
      option.text = gradient;
      option.value = gradients[gradient];
      gradientDropdown.append(option);
    }

    div.append(gradientDropdown);
    return div;
  }

  /**
  * Generates a div with a scale and labels.
  * The div is added to the left corner of the interface.
  */
  addScale() {
    this.scaleDiv = document.createElement("div");
    this.scaleDiv.setAttribute("class", "color_scale_div");
    this.scaleDiv.style.display = "none";

    var scale = document.createElement("div");
    scale.setAttribute("class", "color_scale");
    // A linear gradient will later be set as the background of the div

    // Adds labels to the scale

    var labels = document.createElement("div");
    labels.setAttribute("class", "color_scale_labels");
    var labelTop = document.createElement("div");
    var labelMiddle = document.createElement("div");
    var labelBottom = document.createElement("div");

    labels.append(labelTop, labelMiddle, labelBottom);

    labelTop.innerHTML = "100";
    labelMiddle.innerHTML = "50";
    labelBottom.innerHTML = "0";

    if (this.colorSchemeName == "Hydrophobicity") {
      labelTop.innerHTML = "Hydrophilic";
      labelMiddle.innerHTML = "Neutral";
      labelBottom.innerHTML = "Hydrophobic";
    }

    // Adds a title underneath the scale
    var title = document.createElement("div");
    title.setAttribute("class", "color_scale_title");
    title.innerHTML = this.colorSchemeName;

    this.scaleDiv.append(scale, labels, title);
    document.getElementById("gradients_labels").append(this.scaleDiv);
  }

  /**
  * Generates color parameters for the color scheme and sends information to parent to adjust 3D representation.
  * Also modifies the scale.
  * @todo Color scheme 'RSASA': check the correct scheme (correct frame) is being supplied.
  */
  onColorChange() {
    var gradient = this.colorPickerDiv.querySelector('select').value;

    // Changes the scale
    this.scaleDiv.querySelector('.color_scale').style.background = "linear-gradient(to top, "+getColorsOfGradient(gradient)+")";

    // Defines the color parameters
    var colorParameters = {colorScale: gradient};

    if (this.colorSchemeName == "Hydrophobicity") {
      colorParameters['colorScheme'] = "Hydrophobicity";
    }

    /* Coloring by RSASA is a bit tricky since there is a different color scheme for each frame
    Here, we generate the list of color schemes for the selected gradient
    and use the one for the frame we are at to set the current color parameters.
    The colors parameters are then modified with each frame by the Structure object. */
    if (this.colorSchemeName == "RSASA") {
      // From the selected gradient and the RSASA values, generates a selection-based color scheme per frame
      var schemes = this.getRsasaScheme(gradient);
      // The list of NGL color schemes is sent to the Selection or Structure object from which the color window deprends.
      this.parent.parent.setRsasaColorSchemes(schemes);

      /* We create color parameters for the frame we are at */
      var currentFrame;
      if (this.parent.parent.constructor.name == "Structure") {
        currentFrame = this.parent.parent.NGLtrajectory.trajectory.currentFrame + 1;  // the NGL trajectory starts at -1
      } else {
        currentFrame = this.parent.parent.parent.NGLtrajectory.trajectory.currentFrame + 1;
      }
      colorParameters = {colorScheme: schemes[currentFrame]};
      /* Then, when the trajectory is played, the color parameters will be modified by the Structure object.*/
    }

    this.parent.changeColor(colorParameters);
  }

  /**
  * Called when the user selects a color scheme.
  * Turns the scale visible.
  */
  onColorButtonClick() {
    super.onColorButtonClick();
    this.scaleDiv.style.display = "block";
  }

  /**
  * Given a gradient, generates a list of NGL selection-based color schemes for the color 'RSASA'.
  * @param {str or list} gradient Name of the selected gradient in a format understood by NGL.
  * @return {list} List of NGL color schemes (one for each frame).
  */
  getRsasaScheme(gradient) {
    var colors = getTwentyColorPoints(gradient);
    var schemes = [];

    // Gets RSASA data from the corresponding Structure object
    var rsasaData;
    if (this.parent.parent.constructor.name == "Structure") {
      rsasaData = this.parent.parent.rsasaData;
    } else {
      rsasaData = this.parent.parent.parent.rsasaData;
    }

    /* We include the name of the gradient in the name of the scheme, so that it's saved in the color parameters (we need it for summarizing the color parameters in the information panel) */
    var name = "rsasa|"+gradient;

    // Creates one NGL color scheme per frame
    for (var i = 0; i < rsasaData.length; i++) {
      var scheme = [];
      for (var j = 0; j < 20; j++) {
        scheme.push([colors[j], rsasaData[i][j] ]);
      }
      schemes[i] = NGL.ColormakerRegistry.addSelectionScheme(scheme, name);
    }
    return schemes;
  }
}
