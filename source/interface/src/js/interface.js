/**
* @file Sets the interface up.
* Called from 'interface.php', in which the arrays 'antigen1' and 'antigen2' were defined.
*/

import Structure from './structure.js';
import {greyOut} from './misc.js';
import {checkForIndels, highlightPolymorphisms} from './sequences.js';

"use strict";

/**
* Generates 3D view and fills interface menu and panels.
*/
document.addEventListener( "DOMContentLoaded", function(){
  // Displays a loading animation
  var progressDialog = greyOut();

  // Loading animation is removed once all 3D structures are loaded
  const event = new Event('loaded');
  var loaded = 0; // to count number of structures loaded
  document.addEventListener('loaded', function (e) {
    if (antigen2 == null) {
      progressDialog.remove();
    } else {
      loaded++;
        if (loaded == 2) {
          progressDialog.remove();
        }
    }
  });

  // Creates a NGL stage
  window.stage = new NGL.Stage("viewport");
  window.stage.setParameters( {backgroundColor: "white"} );

  // Creates the Structure object for the first antigen ("main" antigen)
  var main = new Structure(antigen1, "main", event);

  // If there is a second antigen, creates the Structure object for it ("superposed" antigen)
  if (antigen2 != null) {
    var superposed = new Structure(antigen2, "superposed", event);

    // Aligns sequences
    checkForIndels(antigen1, antigen2);
    // Highlights polymorphisms
    highlightPolymorphisms();
  }
});

/**
* Creates general parameters buttons (3 buttons on the bottom right of the stage).
*/

/* To center 3D view */
var centerButton = document.createElement('button');
centerButton.className = "fa_button fas fa-bullseye";
centerButton.setAttribute("title", "Center view");

/* To download a screenshot of the 3D view */
var downloadImageButton = document.createElement('button');
downloadImageButton.className = "fa_button fas fa-camera";
downloadImageButton.setAttribute("title", "Download image");

/* To toggle the background color */
var backgroundColorButton = document.createElement('button');
backgroundColorButton.className = "fa_button fas fa-moon";
backgroundColorButton.setAttribute("title", "Toggle background color");

document.getElementById("general_parameters").append(centerButton, downloadImageButton, backgroundColorButton);

/**
* Function to center view.
*/
centerButton.addEventListener("click", function() {
  window.stage.autoView();
});

/**
* Function to download a high-quality image of the 3D view.
*/
downloadImageButton.addEventListener("click", function() {
  // Name of the image: name of the antigen(s), with asterisks and double dots replaced by hyphens
  var imageName = antigen1.name.replaceAll('*','-').replaceAll(':','-');
  if (antigen2 != null) imageName+= "+"+antigen2.name.replaceAll('*','-').replaceAll(':','-');
  var link = document.createElement('a');
  link.download = imageName+'.png';
  window.stage.makeImage({factor: 4, antialias:true, transparent: true}).then(function(imageBlob) {
    // 'factor: 4' -> image magnified by a factor 4.
    // Transparent background.

    // To save the image on the user's computer:
    link.href = URL.createObjectURL(imageBlob);
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  });
});

/**
* Function to toggle the background color of the stage (between white and black).
*/
backgroundColorButton.addEventListener("click", function() {
  if (this.classList.contains("fa-sun")) {  // to light mode
    this.classList.remove("fa-sun");
    this.classList.add("fa-moon");
    window.stage.setParameters( {backgroundColor: "white"} );

    // Also modifies elements in front of the stage, such as the information panel, the general parameters
    document.documentElement.style.setProperty('--stage-white', "white");
    document.documentElement.style.setProperty('--stage-black', "black");
  } else {  // to dark mode
    this.classList.remove("fa-moon");
    this.classList.add("fa-sun");
    window.stage.setParameters( {backgroundColor: "black"} );

    document.documentElement.style.setProperty('--stage-white', "black");
    document.documentElement.style.setProperty('--stage-black', "white");
  }
});

/**
* Function to show and hide the information panel.
* Listens to clicks on info_panel_button (a little arrow next to the information panel).
*/
document.getElementById("info_panel_button").addEventListener("click", function() {
  var panel = document.getElementById("info_panel");
  if (panel.style.display == "block" || panel.style.display == "") {
    panel.style.display = "none";
    this.style.left = "0px";
    this.classList.remove("fa-chevron-left");
    this.classList.add("fa-chevron-right");
    this.setAttribute("title", "Show information panel");
  } else {
    panel.style.display = "block";
    this.style.left = panel.offsetWidth+"px";
    this.classList.remove("fa-chevron-right");
    this.classList.add("fa-chevron-left");
    this.setAttribute("title", "Hide");
  }
});

/**
* To handle resizing of the window.
*/
window.addEventListener("resize", function() {
  window.stage.handleResize();  // adjusts 3D view
});
