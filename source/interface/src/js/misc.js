/**
* @file Miscallenous functions.
*/

/**
* Removes first instance of an item from a list.
* @param {list} arr List.
* @param {str} value Item to remove from list.
* @return {list} List with one less item.
*/
export function removeItemFromArrayOnce(arr, value) {
  var index = arr.indexOf(value);
  if (index > -1) {
    arr.splice(index, 1);
  }
  return arr;
}

/**
* Provides a loading animation.
* The interface is greyed out with three blinking dots flashing in the center.
* @return {HTMLElement} Div to be displayed.
*/
export function greyOut() {
  var div = document.createElement('div');
  div.setAttribute("class", "opaque");
  div.insertAdjacentHTML("beforeend", "<div class=\"dot-flashing\"></div>");
  document.body.append(div);
  return div;
}

/**
* Displays a dialog with a given message and an "OK" button.
* The dialog closes when the user clicks on the "OK" button or outside the dialog.
* @param {HTMLElement} triggerButton Button on which the user clicks to open the dialog.
* @param {str} messageText Dialog text (HTML format, tags accepted).
*/
export function showMessage(triggerButton, messageText) {
  var dialog = document.createElement('div');
  dialog.setAttribute("class", "info_dialog");
  dialog.insertAdjacentHTML("beforeend", messageText);
  dialog.insertAdjacentHTML("beforeend", "<br>");

  var close = document.createElement('button');
  close.setAttribute("class", "info_message_close_button");
  close.innerHTML = "OK";

  var descendants = dialog.querySelectorAll("*"); // Eventual links, etc.

  dialog.append(close);
  document.body.append(dialog);

  document.addEventListener("click", function(e) {
    var close = true;
    // Only keep the dialog open if the user clicks on the button to open it / inside the div / on an element inside the div (other than the close button)
    if (e.target === triggerButton || e.target === dialog) {
      close = false;
    }
    else {
      for (let i = 0; i < descendants.length; i++) {
        if (e.target === descendants[i]) {
          close = false;
          break;
        }
      }
    }
    if (close) {
      dialog.remove();
    }
  });
}

/**
* Modifies the icon of a button: toggles between an eye ('show') to a slashed eye ('hide').
* @param {HTMLElement} button Button.
*/
export function toggleVisibilityButton(button) {
  if (button.classList.contains("fa-eye")) {
    button.classList.remove("fa-eye");
    button.classList.add("fa-eye-slash");
  } else {
    button.classList.remove("fa-eye-slash");
    button.classList.add("fa-eye");
  }
}

/**
* Checks whether an element has class 'active'.
* @param {HTMLElement} button Button.
* @return {boolean}
*/
export function isButtonActive(button) {
  if (button.classList.contains("active")) {
    return true;
  } else {
    return false;
  }
}

/**
* Modifies class of button: toggles between 'active' and 'inactive'.
* @param {HTMLElement} button Button.
*/
export function toggle_button(button) {
  if (isButtonActive(button)) {
    button.classList.remove("active");
    button.classList.add("inactive");
  } else {
    button.classList.remove("inactive");
    button.classList.add("active");
  }
}

/**
* Dowloads text file. 
* Used to get RSASA data from .csv files.
* @param {str} file_path Path of file on server.
* @return {promise} Promise resolved to text. File contents.
*/
export async function downloadFile(file_path) {
  let response = await fetch(file_path);

  if(response.status != 200) {
    throw new Error("Server Error");
  }

  // Reads response stream as text
  let text_data = await response.text();

  return text_data;
}
