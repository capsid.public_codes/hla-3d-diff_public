/**
* The sequence of the structure being vizualized in the interface is displayed in a panel at the top of the interface.
* Residues selected in the 3D view are highlighted in the sequence.
* If two structures are visualized, both sequences are given, aligned and with their polymorphisms colored.
*
* @file Functions needed for the sequence panel.
*/

import {end_simulated_sequence, indels} from './constants.js';

/**
* Generates the position labels for a sequence, to be added to the sequence panel.
* Every multiple of 10 is marked. Chain names are specified for DQ and DP antigens.
* @param {str} name Chain name if relevant ("Chain A", "Chain B") - "" otherwise
* @param {int} seq_length Sequence length
* @return {str} A series of spans with the sequence positions (raw HTML)
* For each span, the id is: [name]_[position], the position being 1 or a multiple of 10.
*/
export function generateSequencePositions(name, seq_length) {
  var positions_div = "";  // A series of 10-residue long spans

  // The first span shorter: 1-9 residues instead of *0-*9
  if (name != "") { //Chain name if appropriate
      positions_div += "<span class=\"positions_div\" id=\""+name+"_1\" style=\"width:162px\">"+name+"</span>" ;
  } else {
      positions_div += "<span class=\"positions_div\" id=\""+name+"_1\" style=\"width:162px\"> |1 </span>" ;
  }

  // We calculate how many 10-residue long spans we need to display positions for the whole sequence
  var limit = Math.floor((seq_length)/10-1)*10; // floor: rounds down
  // For example, if the sequence is 231 a.a. long, we want 22 spans of tens
  // And the last span will be shorter, only two a.a. long

  // The first span was already added:
  for (var i=10; i<limit+1; i += 10) {
    positions_div += "<span class=\"positions_div\" id=\""+name+"_"+i+"\"> |" + i +"</span>" ;
  }

  // We calculate how many residues, if any, remain
  var remainder = seq_length-limit-10+1;
  if (remainder == 1) {
    // if only one more a.a., no space to label it
    positions_div += "<span class=\"positions_div\" id=\""+name+"_"+i+"\" style=\"width:18px\"> </span>"; // We set the width of the span to the width of the remaining residue
  } else if (remainder > 1) {
    // if more than one a.a., one last label
    positions_div += "<span class=\"positions_div\" id=\""+name+"_"+i+"\" style=\"width:"+remainder*18+"px\"> |"+ i +"</span>";
  }

  return positions_div;
}

/**
* Generates the residues for a sequence (a span per residue), to be added to the sequence panel.
* @param {str} name Sequence name ("seq1" for the main protein, "seq2" for the superposed protein)
* @param {str} sequence_chain_A If there are two chains, sequence of the chain A; else: sequence
* @param {str} sequence_chain_B If there are two chains, sequence of the chain B; else: ""
* @return {str} A series of spans, each for a single residue (raw HTML)
* For each span, the id is: [sequence name]_[chain name if relevant]_[position]
*/
export function generateSequence(name, antigen) {
  var residues_chainA = antigen.sequenceA.split("");
  var sequence = "";
  var group = antigen.group;
  if (group== "DQ" || group == "DR") group += "A1";
  var end_simulated_seq = end_simulated_sequence[group];
  var id = name+"_";
  if (antigen.sequenceB != "") id += "chainA_";
  for (var i = 1; i < residues_chainA.length+1; i++) {
    // Intracellular domains are not included in the structure. We still display them in the sequence, but with a different style.
    if (i <= end_simulated_seq) {
      sequence += "<span class=\"residue\" id=\""+ id + i + "\">" + residues_chainA[i-1] + "</span>";
    } else {
      sequence += "<span class=\"residue missing_residue\" id=\""+ id + i + "\">" + residues_chainA[i-1] + "</span>";
    }
  }
  if (antigen.sequenceB != "") {
  var residues_chainB = antigen.sequenceB.split("");
  var end_simulated_seq = end_simulated_sequence[antigen.group+'B1'];
  id = name+"_chainB_";
  for (var i = 1; i < residues_chainB.length+1; i++) {
      if (i <= end_simulated_seq) {
        sequence += "<span class=\"residue\" id=\""+ id + i + "\">" + residues_chainB[i-1] + "</span>";
      } else {
        sequence += "<span class=\"residue missing_residue\" id=\""+ id + i + "\">" + residues_chainB[i-1] + "</span>";
      }
    }
  }
  return sequence;
}

/**
* Compares the two sequences and colors polymorphisms in red.
*/
export function highlightPolymorphisms() {
  var residuesSeq1 = document.getElementById("sequence_1").querySelectorAll('span');
  var residuesSeq2 = document.getElementById("sequence_2").querySelectorAll('span');
  for (var i = 0; i < residuesSeq1.length; i++) {
    if (residuesSeq1[i].innerHTML != residuesSeq2[i].innerHTML) {
      residuesSeq1[i].classList.add("polymorphism");
      residuesSeq2[i].classList.add("polymorphism");
    }
  }
}

/**
* If needed, aligns the two sequences.
* Uses indels listed in 'constants.js'.
* @todo The function is supposed to hold up to new antigens with new indels being added to the database, but I haven't tested it.
* I've only tested it for the DQ antigens currently in the database.
*/
export function checkForIndels(antigen1, antigen2) {
  /* For DQ and DP antigens, the sequences of both chains are shown: */
  if (["DQ", "DP"].includes(antigen1.group)) {
    var chainA1 = antigen1.name.split("-")[1];
    var chainB1 = antigen1.name.split("-")[0];
    var chainA2 = antigen2.name.split("-")[1];
    var chainB2 = antigen2.name.split("-")[0];
    if (chainA1 != chainA2) { // If we're dealing with two different chains A
      // A deletion found in several DQA1
      var deletion56 = ["DQA1*02", "DQA1*04", "DQA1*05", "DQA1*06"];
      if (deletion56.includes(chainA1.substring(0,7)) && !(deletion56.includes(chainA2.substring(0,7)))) {
        // If the deletion is only present in the reference sequence, add a gap in the reference sequence and the positions labels
        document.getElementById("Chain A_50").style.width = "198px";
        document.getElementById("main_chainA_55").insertAdjacentHTML("afterend", "<span class=\"residue missing_residue\"> - </span>");
      } else if (!(deletion56.includes(chainA1.substring(0,7))) && deletion56.includes(chainA2.substring(0,7))) {
        // If the deletion is only present in the superposed sequence, add a gap in the superposed sequence
        document.getElementById("superposed_chainA_55").insertAdjacentHTML("afterend", "<span class=\"residue missing_residue\"> - </span>");
      }
      // Other indels in the chain A (currently there are none)
      else if (chainA1 in indels) {
        if (indels[chainA1][0] > 0) {
          // Insertion: a gap in the superposed sequence
          for (var i = 0; i < indels[chainA1][0]; i++) {
            document.getElementById("superposed_chainA_"+indels[chainA1][1]).insertAdjacentHTML("afterend", "<span class=\"residue missing_residue\"> - </span>");
          }
        } else {
          // Deletion: a gap in the reference sequence + the positions labels
          for (var i = 0; i < indels[chainA1][0]; i++) {
            document.getElementById("main_chainA_"+indels[chainA1][1]).insertAdjacentHTML("afterend", "<span class=\"residue missing_residue\"> - </span>");
          }
          document.getElementById("Chain A_"+Math.round(indels[chainA1][1]/10)*10).style.width = str(180+(18*indels[chainA1][0]*-1))+"px";
        }
      } else if (chainA2 in indels) {
        if (indels[chainA2][0] > 0) {
          // Insertion: a gap in the reference sequence + the positions labels
          for (var i = 0; i < indels[chainA2][0]; i++) {
            document.getElementById("main_chainA_"+indels[chainA2][1]).insertAdjacentHTML("afterend", "<span class=\"residue missing_residue\"> - </span>");
          }
          document.getElementById("Chain A_"+Math.round(indels[chainA2][1]/10)*10).style.width = str(180+(18*indels[chainA2][0]*-1))+"px";
        } else {
          // Deletion: a gap in the superposed sequence
          for (var i = 0; i < indels[chainA2][0]; i++) {
            document.getElementById("superposed_chainA_"+indels[chainA2][1]).insertAdjacentHTML("afterend", "<span class=\"residue missing_residue\"> - </span>");
          }
        }
      }
    }
    if (chainB1 != chainB2) { // If we're dealing with two different chains B
      if (chainB1 in indels) {
        if (indels[chainB1][0] > 0) {
          // Insertion: a gap in the superposed sequence
          for (var i = 0; i < indels[chainB1][0]; i++) {
            document.getElementById("superposed_chainB_"+indels[chainB1][1]).insertAdjacentHTML("afterend", "<span class=\"residue missing_residue\" disabled> - </span>");
          }
        } else {
          // Deletion: a gap in the reference sequence + the positions labels
          for (var i = 0; i < indels[chainB1][0]; i++) {
            document.getElementById("main_chainB_"+indels[chainB1][1]).insertAdjacentHTML("afterend", "<span class=\"residue missing_residue\" disabled> - </span>");
          }
          document.getElementById("Chain B_"+Math.round(indels[chainB1][1]/10)*10).style.width = str(180+(18*indels[chainB1][0]*-1))+"px";
        }
      } else if (chainB2 in indels) {
        if (indels[chainB2][0] > 0) {
          // Insertion: a gap in the reference sequence + the positions labels
          for (var i = 0; i < indels[chainB2][0]; i++) {
            document.getElementById("main_chainB_"+indels[chainB2][1]).insertAdjacentHTML("afterend", "<span class=\"residue missing_residue\" disabled> - </span>");
          }
          document.getElementById("Chain B_"+Math.round(indels[chainB2][1]/10)*10).style.width = str(180+(18*indels[chainB2][0]*-1))+"px";
        } else {
          // Deletion: a gap in the superposed sequence
          for (var i = 0; i < indels[chainB2][0]; i++) {
            document.getElementById("superposed_chainB_"+indels[chainB2][1]).insertAdjacentHTML("afterend", "<span class=\"residue missing_residue\" disabled> - </span>");
          }
        }
      }
    }
  }
  /* For A, B, C and DR antigens, only the sequence of the alpha chain is shown: */
  else {
    if (antigen1.name in indels) {
      if (indels[antigen1.name][0] > 0) {
        // Insertion: a gap in the superposed sequence
        for (var i = 0; i < indels[antigen1.name][0]; i++) {
          document.getElementById("superposed_"+indels[antigen1.name][1]).insertAdjacentHTML("afterend", "<span class=\"residue missing_residue\" disabled> - </span>");
        }
      } else {
        // Deletion: a gap in the reference sequence + the positions labels
        for (var i = 0; i < indels[antigen1.name][0]; i++) {
          document.getElementById("main_"+indels[antigen1.name][1]).insertAdjacentHTML("afterend", "<span class=\"residue missing_residue\" disabled> - </span>");
        }
        document.getElementById(Math.round(indels[antigen1.name][1]/10)*10).style.width = str(180+(18*indels[antigen1.name][0]*-1))+"px";
      }
    } else if (antigen2.name in indels) {
      if (indels[antigen2.name][0] > 0) {
        // Insertion: a gap in the reference sequence + the positions labels
        for (var i = 0; i < indels[antigen2.name][0]; i++) {
          document.getElementById("main_"+indels[antigen2.name][1]).insertAdjacentHTML("afterend", "<span class=\"residue missing_residue\" disabled> - </span>");
        }
        document.getElementById(Math.round(indels[antigen2.name][1]/10)*10).style.width = str(180+(18*indels[antigen2.name][0]*-1))+"px";
      } else {
        // Deletion: a gap in the superposed sequence
        for (var i = 0; i < indels[antigen2.name][0]; i++) {
          document.getElementById("superposed_"+indels[antigen2.name][1]).insertAdjacentHTML("afterend", "<span class=\"residue missing_residue\" disabled> - </span>");
        }
      }
    }
  }

}
