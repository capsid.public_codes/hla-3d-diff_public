/**
* @file Manages Structure objects.
* One antigen equals one Structure object. It corresponds to one 3D structure and one molecular dynamics (MD) simulation.
* For each, there is a menu which enables the user to manage the 3D structure, play the MD simulation, make selections.
* @module Structure
*/

import {ResiduesSelection, EpletsSelection, PatchSelection} from './selection.js';
import {RepresentationWindow, ColorWindow} from './style.js';
import {toggleVisibilityButton, downloadFile} from './misc.js';
import {getColorSummary} from './colors.js';
import {end_simulated_sequence, peptideBindingCleftPositionClassI, peptideBindingCleftPositionClassII, transmembraneRegionPositionClassI, transmembraneRegionPositionClassII} from './constants.js';
import {generateSequencePositions, generateSequence} from './sequences.js';

export default class Structure {
  antigen;  /* array, see 'interface.php' (function get_structure_data()) for details */
  id; /* "main" or "superposed" */

  NGLstructure; /* NGL structure component */
  NGLtrajectory; /* NGL trajectory component */

  /* A Structure object has a div in the interface menu, with 3 buttons to manage the 3D structure,
  a player for the MD simulation, and a dropdown in which selection 'blocks' can be added.*/
  div;
  showButton; /* for the user to toggle the visibility of the 3D structure */
  representationButton; /* for the user to manage the representations of the 3D structure */
  colorButton; /* for the user to manage the color parameters of the 3D structure */

  playerButton; /* for the user to start and stop the MD simulation */
  slider; /* for the MD simulation */
  sliderValue; /* to give the frame the simulation is currently at */
  nanoseconds; /* the frame's correspondance in nanoseconds */

  residuesSelectionDiv; /* dropdown in which ResiduesSelection objects can be added */
  epletsSelectionDiv; /* dropdown in which EpletsSelection objects can be added */
  patchesSelectionDiv; /* dropdown in which PatchSelection objects can be added */

  reprDict = []; /* array of NGL representation components, corresponds to currently selected and displayed representations (e.g. a 'cartoon' representation and a 'licorice' representation) */
  currentColorParameters; /* current color parameters, in NGL syntax (array) */

  currentRsasaColorSchemes; /* If RSASA data is available for the antigen, array containing the color schemes corresponding to the data. */
  /* (For each frame of the MD simulation there is a specific color scheme.) */
  /* (These color schemes can vary - they depend on the gradient picked by the user.) */

  playing = false; /* boolean, indicates whether the MD simulation is playing */

  selections = [[], [], []];  /* arrays of residues selections, eplets selections, patch selections (Selection objects) */

  infoDiv; /* designated div in the info panel, to display details about the structure and a summary of the possible selection */

  rsasaData = []; /* array of arrays, each array corresponds to a frame and contains the lists of residues to use to generate custom selection-based color schemes */

  /**
  * Generates a Structure object according to the data in an antigen object.
  * @param {object} antigen Antigen.
  * @param {str} id "main" or "superposed"
  * @param {Event} event Event to be fired when the different HTML elements are generated and the structural files are loaded.
  */
  constructor(antigen, id, event) {
    this.antigen = antigen;
    this.id = id;

    // Generates a div dedicated to the given structure in the interface menu
    this.generateHTMLElement();
    // Adds the structure's sequence to the sequence panel
    this.addSequence();
    // Adds a div in the info panel, to display details about the structure
    this.addInformationDiv();

    // Loads the 3D structure, loads the MD simulations, adds the eventlisteners for the various menu buttons
    // (Note: There's probably a better way to do this than nesting it like this but it works)
    let self = this;
    window.stage.loadFile(self.antigen.path2pdbfile)  // Loads structure (pdb file)
    .then(function(structure) {
      self.NGLstructure = structure;
      structure.setName(self.id);
      structure.autoView();

      NGL.autoLoad(self.antigen.path2dcdfile).then(function(frames) { // Loads trajectory (dcd file)
        self.NGLtrajectory = structure.addTrajectory(frames);

        // Adds the eventlisteners for the various menu buttons
        self.addListeners();  // Done after the trajectory is loaded, as we need to link it to the player

        // Adds buttons to view labels for the peptide binding cleft and the transmembrane region on the 3D structure
        if (self.id == "main") self.addLabels();

        // Fires an event to remove the loading animation and let the user use the interface
        document.dispatchEvent(event);

        // If there is RSASA data available, downloads it
        // (Added at the end as it can take some time, and shouldn't immediately be necessary)
        if (self.antigen.sasa != null) {
          self.downloadRsasaData();
        }

        // TODO:
        // If temporary files were generated, deletes them
        // if (self.id == "superposed") {}
      })
    });
  }

  /**
  * Creates the div dedicated to the structure in the interface menu (HTML only).
  */
  generateHTMLElement() {
    // Main div
    this.div = document.createElement('div');
    this.div.setAttribute("class", "structure_block");

    // Title
    var titleDiv = document.createElement('div');
    titleDiv.innerHTML = this.antigen.name;
    titleDiv.setAttribute("class", "structure_title");
    this.div.append(titleDiv);

    // Buttons for the whole structure: show/hide, representations, colors
    this.div.append(this.generateParametersButtons());

    // Player for the MD simulation
    this.div.append(this.generatePlayerDiv());

    // Dropdowns in which selection 'blocks' can be added
    // One dropdown for each type of selection: residues, eplets, patches
    var selectionDiv = document.createElement('div');
    selectionDiv.setAttribute("class", "selection_div");
    this.residuesSelectionDiv = this.generateSelectionDiv("Select residues");
    this.epletsSelectionDiv = this.generateSelectionDiv("Select eplets");
    this.patchesSelectionDiv = this.generateSelectionDiv("Select patches");
    selectionDiv.append(this.residuesSelectionDiv, this.epletsSelectionDiv, this.patchesSelectionDiv);
    this.div.append(selectionDiv);

    document.getElementById("menu").append(this.div);
  }

  /**
  * Generates 3 buttons to manage the 3D structure (HTML only).
  * One button to toggle the structure's visibility, one to manage its representations, and one to manage its color parameters.
  * @return {HTMLElement} Div with the 3 buttons.
  */
  generateParametersButtons() {
    var buttonsDiv = document.createElement('div');
    buttonsDiv.setAttribute("class", "buttons_for_the_whole_structure");

    // Generate a button to toggle the visibility of the structure
    this.showButton = document.createElement('button');
    this.showButton.className = "fa_button fas fa-eye";

    // Generate a button to change the representation of the structure
    this.representationButton = document.createElement('button');
    this.representationButton.className = "fa_button fas fa-project-diagram";

    // Generate a button to change the color of the structure
    this.colorButton = document.createElement('button');
    this.colorButton.className = "fa_button fas fa-palette";

    buttonsDiv.append(this.showButton);
    buttonsDiv.insertAdjacentHTML("beforeend", "<span class=\"vertical_line_1\"> </span>");
    buttonsDiv.append(this.representationButton);
    buttonsDiv.insertAdjacentHTML("beforeend", "<span class=\"vertical_line_1\"> </span>");
    buttonsDiv.append(this.colorButton);

    return buttonsDiv;
  }

  /**
  * Generates a 'player' to play the MD simulation (HTML only).
  * It consists of a 'Play / Pause' button, a slider, and an indication of the selected frame.
  * @return {HTMLElement} Div with the player.
  */
  generatePlayerDiv() {
    var playerDiv = document.createElement('div');
    playerDiv.setAttribute("class", "player_div");

    // 'Play / Pause' button
    this.playerButton = document.createElement('button');
    this.playerButton.className = "fa_button fas fa-play";
    this.playerButton.setAttribute("title", "Play / Pause");

    // Slider
    this.slider = document.createElement('input');
    this.slider.setAttribute("class", "slider");
    this.slider.setAttribute('type', 'range');
    this.slider.setAttribute('min', 0);
    this.slider.setAttribute('max', this.antigen.nber_frames);
    this.slider.setAttribute('value', 0);

    // Displays the frame of the MD simulation and its correspondance in nanoseconds
    this.sliderValue = document.createElement('span');
    this.sliderValue.innerHTML = "0";
    this.nanoseconds = document.createElement('span');
    this.nanoseconds.innerHTML = "0";

    playerDiv.append(this.playerButton);
    playerDiv.append(this.slider);
    playerDiv.insertAdjacentHTML("beforeend", "<span>Frame </span>");
    playerDiv.append(this.sliderValue);
    playerDiv.insertAdjacentHTML("beforeend", "<span> / "+this.antigen.nber_frames+" - </span>");
    playerDiv.append(this.nanoseconds);
    playerDiv.insertAdjacentHTML("beforeend", "<span> ns</span>"); // unit (nanoseconds)

    return playerDiv;
  }

  /**
  * Generates a dropdown in which selection 'blocks' can be added.
  * @param {str} title Title of the dropdown: one of 'Select residues', 'Select eplets', 'Select patches'.
  * @return {HTMLElement} Div.
  */
  generateSelectionDiv(title) {
    var div = document.createElement('div');

    var titleDiv = document.createElement('div');
    titleDiv.setAttribute("class", "selection_div_title");

    titleDiv.insertAdjacentHTML("beforeend", title);
    var chevron = document.createElement('button');
    chevron.className = "fa_button fas fa-chevron-down";
    titleDiv.append(chevron);

    var dropdownDiv = document.createElement('div');
    dropdownDiv.setAttribute("class", "selection_block_dropdown");
    dropdownDiv.style.display = "none";

    titleDiv.addEventListener("click", function() {
      if (chevron.classList.contains("fa-chevron-down")) {
          chevron.classList.remove("fa-chevron-down");
          chevron.classList.add("fa-chevron-up");
          dropdownDiv.style.display = "block";
      } else {
          chevron.classList.remove("fa-chevron-up");
          chevron.classList.add("fa-chevron-down");
          dropdownDiv.style.display = "none";
      }
    });

    /* In each dropdown, there is an 'Add' button to enable the user to add selection blocks */

    var addButton = document.createElement('div');
    addButton.setAttribute("class", "add_selection_button");
    addButton.innerHTML = "Add " + "<button class = \"fa_button fas fa-plus\"> </button>";

    dropdownDiv.append(addButton);

    let self = this;

    if (title == "Select residues") {
      addButton.addEventListener("click", function(){self.addResiduesSelection(dropdownDiv)});
    } else if (title == "Select eplets") {
      addButton.addEventListener("click", function(){self.addEpletsSelection(dropdownDiv)});
    } else {
      addButton.addEventListener("click", function(){self.addPatchesSelection(dropdownDiv)});
    }

    div.append(titleDiv);
    div.append(dropdownDiv);

    return div;
  }

  /**
  * Adds a div in the info panel, to display details about the structure.
  */
  addInformationDiv() {
    this.infoDiv = document.createElement('div');
    this.infoDiv.setAttribute("class", "info_panel_structure_div");

    var titleDiv = document.createElement('div');
    this.infoDiv.append(titleDiv);

    // Displays the name of the antigen
    var title = document.createElement('span');
    title.setAttribute("class", "info_panel_structure_title");
    title.insertAdjacentHTML("beforeend", this.antigen.name);
    titleDiv.append(title);

    // Shows a summary of the 3D structure's color parameters
    var colorSummary = document.createElement('span');
    colorSummary.setAttribute("class", "structure_color_summary");
    titleDiv.append(colorSummary);

    document.getElementById("info_panel").append(this.infoDiv);
  }

  /**
  * Adds eventlisteners to the 3 parameters buttons and the elements of the trajectory player.
  */
  addListeners() {
    let self = this;

    this.showButton.addEventListener("click", function(){self.toggleStructureVisibility()});

    var representationWindow = new RepresentationWindow(self, self.representationButton, self.id);
    this.representationButton.addEventListener("click", function(){representationWindow.toggleWindowVisibility()});

    var colorWindow = new ColorWindow(self, self.colorButton, self.id);
    this.colorButton.addEventListener("click", function(){colorWindow.toggleWindowVisibility()});

    var player = new NGL.TrajectoryPlayer(
      self.NGLtrajectory.trajectory, { step: 1, timeout: 200 } // timeout: "how many milliseconds to wait between playing frames"
    );

    self.NGLtrajectory.signals.frameChanged.add(function(){
         var fnum = self.NGLtrajectory.trajectory.currentFrame;
         self.slider.value = fnum;
         self.sliderValue.innerHTML = fnum;
         self.nanoseconds.innerHTML = self.getNanoseconds(fnum);

         // Checks if the structure and/or a selection is set to be colored by RSASA
         // and if so, updates the relevant color schemes
         if (self.antigen.sasa != null) {
           self.updateRsasaSchemes();
         }

     });

    // Manages user manually moving the slider
    this.slider.addEventListener("input", function() {
      var frame = this.value;
      self.sliderValue.innerHTML = frame;
      self.nanoseconds.innerHTML = self.getNanoseconds(frame);
      if (frame == self.antigen.nber_frames) frame = self.antigen.nber_frames - 1; // for some reason setting the trajectory at the last frame results in an error with NGL
      self.NGLtrajectory.setFrame(frame);
    });

    // Manages user clicking on the "play / pause" button
    this.playerButton.addEventListener("click", function() {
      if( !self.playing ){
        player.play();
        self.playing = true;
        this.classList.remove("fa-play");
        this.classList.add("fa-pause");
      } else {
        player.pause();
        self.playing = false;
        this.classList.remove("fa-pause");
        this.classList.add("fa-play");
      }
    });
  }

  /**
  * Creates buttons that enable the user to label the peptide binding cleft and the transmembrane region on the 3D structure.
  * The buttons are two checkboxes that are displayed on the bottom left of the interface.
  */
  addLabels() {
    // Peptide binding cleft
    var peptideBindingCleft = document.createElement("div");
    var checkbox1 = document.createElement("input");
    checkbox1.setAttribute("type", "checkbox");
    checkbox1.setAttribute("id", "peptide_binding_cleft_label");
    var checkboxLabel1 = document.createElement("label");
    checkboxLabel1.setAttribute("for", "peptide_binding_cleft_label");
    checkboxLabel1.innerHTML = "Peptide binding cleft";
    peptideBindingCleft.append(checkbox1, checkboxLabel1);

    var label1 = document.createElement("div");
    label1.setAttribute("class", "label_on_structure");
    label1.innerHTML = "Peptide binding cleft";
    // Uses a approximate position (constants.js) to center the label
    var selectionCenter1;
    if (["A", "B", "C"].includes(this.antigen.group)) {
      selectionCenter1 = this.NGLstructure.structure.atomCenter(new NGL.Selection(peptideBindingCleftPositionClassI));
    } else {
      selectionCenter1 = this.NGLstructure.structure.atomCenter(new NGL.Selection(peptideBindingCleftPositionClassII));
    }
    var annotation1 = this.NGLstructure.addAnnotation(selectionCenter1, label1);
    annotation1.setVisibility(false);

    checkbox1.addEventListener("change", function() {
      if (this.checked) {
        annotation1.setVisibility(true);
      } else {
        annotation1.setVisibility(false);
      }
    });

    // Transmembrane region (start of)
    var transmembraneRegion = document.createElement("div");
    var checkbox2 = document.createElement("input");
    checkbox2.setAttribute("type", "checkbox");
    checkbox2.setAttribute("id", "transmembrane_region_label");
    var checkboxLabel2 = document.createElement("label");
    checkboxLabel2.setAttribute("for", "transmembrane_region_label");
    checkboxLabel2.innerHTML = "Transmembrane region (not shown)";
    transmembraneRegion.append(checkbox2, checkboxLabel2);

    var label2 = document.createElement("div");
    label2.setAttribute("class", "label_on_structure");
    label2.innerHTML = "Transmembrane region";
    var selectionCenter2;
    if (["A", "B", "C"].includes(this.antigen.group)) {
      selectionCenter2 = this.NGLstructure.structure.atomCenter(new NGL.Selection(transmembraneRegionPositionClassI));
    } else {
      selectionCenter2 = this.NGLstructure.structure.atomCenter(new NGL.Selection(transmembraneRegionPositionClassII));
    }

    var annotation2 = this.NGLstructure.addAnnotation(selectionCenter2, label2);
    annotation2.setVisibility(false);

    checkbox2.addEventListener("change", function() {
      if (this.checked) {
        annotation2.setVisibility(true);
      } else {
        annotation2.setVisibility(false);
      }
    });

    document.getElementById("general_labels").append(peptideBindingCleft,transmembraneRegion);
  }

  /**
  * Adds a selection 'block' (ResiduesSelection object).
  * @param {HTMLElement} div Indicates where in the menu the 'block' should be inserted.
  */
  addResiduesSelection(div) {
    var selection = new ResiduesSelection(this);
    this.selections[0].push(selection);
    div.insertBefore(selection.div, div.children[div.children.length-1]); // insert in the second-to-last position (just before the button to add another selection)
  }

  /**
  * Adds a selection 'block' (EpletsSelection object).
  * @param {HTMLElement} div Indicates where in the menu the 'block' should be inserted.
  */
  addEpletsSelection(div) {
    var selection = new EpletsSelection(this);
    this.selections[1].push(selection);
    div.insertBefore(selection.div, div.children[div.children.length-1]);
  }

  /**
  * Adds a selection 'block' (PatchSelection object).
  * @param {HTMLElement} div Indicates where in the menu the 'block' should be inserted.
  */
  addPatchesSelection(div) {
    var selection = new PatchSelection(this);
    this.selections[2].push(selection);
    div.insertBefore(selection.div, div.children[div.children.length-1]);
  }

  /**
  * Toggles the visibility of the 3D structure and possible selections.
  */
  toggleStructureVisibility() {
    toggleVisibilityButton(this.showButton);

    // Hides / shows all representations of the structure
    for(let reprName in this.reprDict) {
      this.reprDict[reprName].toggleVisibility();
    }

    // Hides visible selections / shows hidden selections as well
    for (var j in [3,2,1]) {
      for (var i = 0; i < this.selections[j].length; i++) {
        this.selections[j][i].toggleSelectionVisibility(true);
      }
    }
  }

  /*
  * Adds the sequence of the antigen in the sequence panel, as well as the position labels if necessary (if it's the first sequence added).
  */
  addSequence() {
    if (this.id == "main") {
      // Positions
      if (this.antigen.group== "DQ" || this.antigen.group == "DR") { // Two chains
        document.getElementById("positions").innerHTML = generateSequencePositions("Chain A", this.antigen.sequenceA.length) +
          generateSequencePositions("Chain B", this.antigen.sequenceB.length);
      } else {
        document.getElementById("positions").innerHTML = generateSequencePositions("", this.antigen.sequenceA.length);
      }
      // Label (will only be displayed if there are two sequences)
      document.getElementById("sequence_names").insertAdjacentHTML("beforeend", "<div>"+this.antigen.name+"</div>");
      // Sequence
      document.getElementById("sequence_1").innerHTML = generateSequence(this.id, this.antigen);
    } else {
      // Label
      document.getElementById("sequence_names").insertAdjacentHTML("beforeend", "<div>"+this.antigen.name+"</div>");
      document.getElementById("sequence_names").style.display = "flex";
      // Sequence
      document.getElementById("sequence_2").innerHTML = generateSequence(this.id, this.antigen);
    }
  }

  /**
  * Adds a new representation for the structure.
  * @param {str} reprName Name of the representation as understood by NGL, e.g. 'cartoon'.
  * @param {float} opacityValue Opacity. 1 is fully opaque, 0 is fully transparent.
  */
  addRepr(reprName, opacityValue) {
    var newRepr = this.NGLstructure.addRepresentation(reprName, {opacity: opacityValue, opaqueBack: false});
    newRepr.setParameters(this.currentColorParameters);
    this.reprDict[reprName] = newRepr;
    this.generateAllSelectionsAgain();  // so that the new representation won't hide the selections
  }

  /**
  * Deletes a representation for the structure.
  * @param {str} reprName Name of the representation as understood by NGL, e.g. 'cartoon'.
  */
  deleteRepr(reprName) {
    this.reprDict[reprName].dispose();
  }

  /**
  * Modifies the opacity of a representation.
  * @param {str} reprName One of the current representations for the structure. Name of the representation as understood by NGL, e.g. 'cartoon'.
  * @param {float} opacityValue Opacity. 1 is fully opaque, 0 is fully transparent.
  */
  changeReprOpacity(reprName, opacityValue) {
    this.reprDict[reprName].setParameters({opacity: opacityValue});
  }

  /**
  * Modifies the color parameters of the structure.
  * @param {object} colorParameters Color parameters in the NGL syntax.
  * @param {boolean} differentColorScheme True if the user selected new color parameters (default), false otherwise.
  * 'differentColorScheme' is for when the structure is colored by RSASA, as the color parameters change with every frame, but the summary doesn't need to be updated.
  */
  changeColor(colorParameters, differentColorScheme = true) {
    for(var repr in this.reprDict) {
        this.reprDict[repr].setParameters(colorParameters);
    }
    if (differentColorScheme) {
      this.currentColorParameters = colorParameters;
      this.infoDiv.querySelector(".structure_color_summary").innerHTML = "";
      this.infoDiv.querySelector(".structure_color_summary").append(getColorSummary(colorParameters));
    }
  }

  /**
  * Re-creates all representations for all selections as they were.
  * Called when a new representation for the whole structure is added, or when a new selection is made,
  * because otherwise previously made selections could end up hidden.
  */
  generateAllSelectionsAgain() {
    // First the patches, then the eplets, then the residues
    // (arbitrary order, according to the likely size of selections)
    for (var j in [3,2,1]) {
      for (var i = 0; i < this.selections[j].length; i++) {
        this.selections[j][i].rebuildRepresentations();
      }
    }
  }

  /**
  * Given a frame, calculates its correspondance in nanoseconds.
  * @param {int} frame Frame number.
  * @return {float} Correspondance in nanoseconds.
  */
  getNanoseconds(frame) {
    return Math.round((this.antigen.run_duration / this.antigen.nber_frames) * frame * 100) / 100;
  }

  /**
  * Dowloads RSASA data for the structure and transforms it to be used to generate selection-based color schemes.
  * Appends result to this.rsasaData.
  */
  downloadRsasaData() {
    /* In the .csv files, there is one column per frame, and one line per residue.
    The first line and column are indexes. */

    /* We will put all these RSASA values into an array of arrays: rsasaValues[frame][residue] (same structure as in the .csv file)*/
    var rsasaValues;

    console.log("Downloading RSASA data");
    let text_data = downloadFile(this.antigen.sasa);
    let self = this;
    text_data.then(function(data) { // once the file is dowloaded
      var arrayOfRsasaValuesForAResidue = data.split("\n");
      rsasaValues = new Array(arrayOfRsasaValuesForAResidue[1].split("\t").length-1); // sets the length of rsasaValues to the number of frames
      // Looping through residues:
      for (var i = 1; i < arrayOfRsasaValuesForAResidue.length; i++) {  // we ignore the first line (index)
        var rsasaValuesForAResidue = arrayOfRsasaValuesForAResidue[i].split("\t");
        // Looping through frames:
        for (var j = 1; j < rsasaValuesForAResidue.length; j++) { // we ignore the first column (index)
          if (i == 1) {
            rsasaValues[j-1] = []; // creates array
          }
          rsasaValues[j-1].push(rsasaValuesForAResidue[j]);
        }
      }

      console.log("Grouping residues by their RSASA values");
      self.rsasaData = self.clusterRsasaData(rsasaValues);
    });
  }

  /**
  * Prepares for the generation of selection-based color schemes by grouping residues by their RSASA values.
  * For each frames, clusters residues based on their RSASA values and generates 20 lists of positions to be colored together.
  * @param {array of arrays of floats} rsasaValues RSASA values for all frames of the trajectory (rsasaValues[frame][residue]).
  * @return {array of arrays of strings} For each frame, array of the selections to use to generate custom color schemes. (arrayOfRsasaSelections[frame][0-20])
  */
  clusterRsasaData(rsasaValues) {
    /* To color the structure according to the RSASA values, we need to generate NGL color schemes (one color scheme per frame).
    A selection-based color scheme will take parameters like this:
          [ ["red", "64-74 or 134-154 or 222-254 or 310-310 or 322-326"],
          ["green", "311-322"],
          ["yellow", "40-63 or 75-95 or 112-133 or 155-173 or 202-221 or 255-277 or 289-309"],
          ["blue", "1-39 or 96-112 or 174-201 or 278-288"],
          ["white", "*"]  ]
    (See 'selection-based coloring': http://nglviewer.org/ngl/api/manual/coloring.html).
    The actual color schemes will depend on the gradient selected by the user, but the lists of selections will always be the same.

    For each frame, we cluster residues into selections based on their RSASA values.
    I went with 20 selections. */

    var arrayOfRsasaSelections = []; /* one per frame */

    var numberOfIntervals = 20;
    var interval = 100 / numberOfIntervals; /* RSASA values are between 0 and 100 */
    var residuePosition;

    // For each frame:
    for (var i = 0; i < rsasaValues.length; i++) {
      var selections = new Array(numberOfIntervals);  /* arrays of residues */

      // Creates arrays:
      for (var j = 0; j < numberOfIntervals; j++) {
        selections[j] = [];
      }

      // For each residue:
      for (var j = 0; j < rsasaValues[i].length; j++) {
        residuePosition = j+1;
        if (residuePosition > end_simulated_sequence[this.antigen.group]) {
          residuePosition = (residuePosition - end_simulated_sequence[this.antigen.group]).toString() + ":B";
        } else {
          residuePosition = residuePosition.toString() + ":A";
        }
        // Append residue position to correct selection
        for (var k = 0; k < (100 - interval) ; k += interval) {  /* RSASA values are (supposed to be) between 0 and 100 */
          if (rsasaValues[i][j] >= k && rsasaValues[i][j] < k+5) {
            selections[(k/interval)].push(residuePosition);
          }
        }
        if (rsasaValues[i][j] >= 95) {  // because sometimes there are values slightly above 100 (like 100.8)
          selections[19].push(residuePosition);
        }
      }

      var selectionsInNglSyntax = []; /* e.g. "1 or 10 or 25" */
      for (var j = 0; j < numberOfIntervals; j++) {
        if (selections[j].length != 0) {
          var selection = "";
          for (var k = 0; k < selections[j].length - 1; k++) {
            selection += selections[j][k] + " or ";
          }
          selection += selections[j][selections[j].length-1]; // so as not to have an " or " at the end
          selectionsInNglSyntax[j] = selection;
        } else {  // no residues in selection
          selectionsInNglSyntax[j] = "0"; // an empty string would cause an error
        }
      }
      arrayOfRsasaSelections[i] = selectionsInNglSyntax;
     }
     return arrayOfRsasaSelections;
  }

  /**
  * Updates the color parameters of elements colored by RSASA.
  * Called everytime a frame is changed, checks if the structure and/or one of its selection is set to be colored by RSASA,
  * and if so, updates their color parameters to fit the current frame.
  * @todo function not entirely tested yet
  * @todo Check the correct scheme (correct frame) is being supplied.
  * @todo Manage selections colored by RSASA.
  */
  updateRsasaSchemes() {
    // Is the color scheme for the whole structure set to RSASA?
    if (this.currentColorParameters.colorScheme.includes("rsasa")) { // the actual name of the color scheme would be [random name]|rsasa|colorscheme
      this.changeColor({colorScheme: this.currentRsasaColorSchemes[this.NGLtrajectory.trajectory.currentFrame]}, false);
    }

    // TODO:
    // add variable currentRsasaColorSchemes to Selection objects, create schemes when the user picks 'RSASA' as a color scheme,
    // go through schemes as the trajectory plays
    // (similar to what is done for the Structure object)

    // Is the color scheme for any of the selections set to RSASA?
    // for (var i in [1,2,3]) {
    //   for (var j = 0; j < this.selections[i].length; j++) {
    //     if (this.selections[i][j].currentColorParameters.includes("rsasa")) {
    //
    //     }
    //   }
    // }
  }

  /**
  * Sets the color schemes to be used to color the structure by RSASA.
  * @param {array} schemes Contains the color schemes for all frames of the simulation for the selected gradient.
  */
  setRsasaColorSchemes(schemes) {
    this.currentRsasaColorSchemes = schemes;
  }

}
