<?php
function set_db_connection() {
  $servername = "db"; // container
  $username = getenv('MYSQL_USER');
  $password = getenv('MYSQL_PASSWORD');
  $database = getenv('DATABASE_NAME');

  // Create connection
  $connection = new mysqli($servername, $username, $password, $database);

  // Check connection
  if ($connection->connect_error) {
    die("Connection failed: " . $connection->connect_error);
  }

  return $connection;
}
?>
