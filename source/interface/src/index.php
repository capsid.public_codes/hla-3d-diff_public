<!--
Homepage of the HLA-3D-Diff visualisation interface
Enables user to select one or two antigens to visualize.
-->

<?php
include "mysql.php";
?>

<!DOCTYPE html>
<html>
<head>
    <title>HLA-3D-Diff - visualisation interface</title>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>

  <h1>HLA-3D-Diff visualisation interface</h1>

  <form method="get" action="interface.php">
    <label for="structure1">Select an antigen to visualize: </label>
    <select id="structure1" name="structure1">
      <option value="" disabled selected hidden> </value> <!-- placeholder -->
    </select>
    <br/> <br/>
    <label for="structure2">Select a second antigen to superpose (optional): </label>
    <select id="structure2" name="structure2">
    </select>
    <br/> <br/>
    <input type="submit">
  </form>

</body>

<?php
  $connection = set_db_connection();
  $query_antigens = "SELECT Antigen.ag_name FROM Antigen
      LEFT JOIN Exp_structure ON Exp_structure.ag_name = Antigen.ag_name
      LEFT JOIN Mod_structure ON Mod_structure.ag_name = Antigen.ag_name
      JOIN MD_run ON MD_run.exp_structure_id = Exp_structure.exp_structure_id OR MD_run.mod_structure_id = Mod_structure.mod_structure_id
      WHERE MD_run.used_in_visu = 1
      ORDER BY ag_group ASC, Antigen.ag_name ASC";
   $list_antigens = mysqli_query($connection, $query_antigens);
   $array_antigens = array();
   while ($antigen = mysqli_fetch_array($list_antigens, MYSQLI_NUM)) {
     $array_antigens[] = $antigen[0];
   }
  mysqli_free_result($list_antigens);
  mysqli_close($connection);
?>

<script>
  var antigens = <?php echo json_encode($array_antigens); ?>;

  var structure1 = document.getElementById("structure1");
  var structure2 = document.getElementById("structure2");

  // Adds available antigens to the first selection dropdown
  for (var i in antigens) {
    structure1.insertAdjacentHTML("beforeend", "<option value =\""+antigens[i]+"\">"+antigens[i]+"</option>");
  }

  // Once an antigen is selected in the first dropdown, adds antigens that can be superposed to it in the second dropdown
  structure1.addEventListener("change", function() {
    structure2.innerHTML = "<option value=\"none\" selected> </value>";
    for (var i in antigens) {
      // Checks if antigen in the same 'group' (A, B, C, DR, DQ, DP) as first antigen + isn't first antigen
      if ( this.value != antigens[i] && ( (['A', 'B', 'C'].includes(this.value.substring(0,1)) && antigens[i].substring(0,1) == this.value.substring(0,1))
          || (['DR', 'DQ', 'DP'].includes(this.value.substring(0,2)) && antigens[i].substring(0,2) == this.value.substring(0,2)) ) ) {
        structure2.insertAdjacentHTML("beforeend", "<option value =\""+antigens[i]+"\">"+antigens[i]+"</option>");
      }
    }
  });

</script>
