#!/usr/bin/python3

import subprocess
from flask import Flask, request
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

@app.route("/align_with_vmd", methods=["POST"])
def run_vmd_command():
    """Runs align_DCDs.tcl script using VMD."""

    json_data = request.get_json()

    psf1 = json_data["path_psf1"]   # path of the .psf file for the first structure
    dcd1 = json_data["path_dcd1"]   # path of the .dcd file for the first structure
    psf2 = json_data["path_psf2"]   # path of the .psf file for the second structure
    dcd2 = json_data["path_dcd2"]   # path of the .dcd file for the second structure
    output = json_data["output_name"]   # name of the output files (without path or extensions)

    vmd_command = "vmd -dispdev text -e /src/align_DCDs.tcl -args "+psf1+" "+dcd1+" "+psf2+" "+dcd2+" "+output;
    # Aligns a MD simulation to a reference simulation, writes .pdb and .dcd files to ./tmp/ (shared between containers as 'TMP_DIRECTORY').

    processes = subprocess.run(vmd_command.split(), capture_output=True, text=True)
    print(processes.stdout)
    print(processes.stderr)

    return processes.stdout

app.run(host='0.0.0.0', debug=True, port=8080)
