# USAGE : vmd -dispdev text -e align_DCDs.tcl -args psf1 dcd1 psf2 dcd2 output_name
# Aligns a MD simulation (psf2, dcd2) to a reference simulation (psf1, dcd1)
# Output files (a .pdb and a .dcd, as needed for the visualisation interface) are written in the ./tmp/ folder which is shared between containers as 'TMP_DIRECTORY'

package require psfgen
package require membrane

set psf1 [lindex $argv 0]
set dcd1 [lindex $argv 1]
set psf2 [lindex $argv 2]
set dcd2 [lindex $argv 3]
set out [lindex $argv 4]

set mol1 [mol new $psf1 waitfor all]
mol addfile $dcd1 waitfor all $mol1
set mol2 [mol new $psf2 waitfor all]
mol addfile $dcd2 waitfor all $mol2


set prefix1 [string range [file tail $psf1] 0 1]
set prefix2 [string range [file tail $psf2] 0 1]



if { $prefix1 == "DQ" || $prefix2 == "DQ" } {

  set chainB [atomselect $mol1 "chain B"]
  set residList [$chainB get resid]
  set start_B_mol1 [lindex [lsort -integer -increasing $residList] 0]

  set chainB [atomselect $mol2 "chain B"]
  set residList [$chainB get resid]
  set start_B_mol2 [lindex [lsort -integer -increasing $residList] 0]

  set last_B_mol1 [expr {$start_B_mol1 + 191}]
  set last_B_mol2 [expr {$start_B_mol2 + 191}]

  set selection1 "(chain A and resid 1 to 186 and name CA) or (chain B and resid $start_B_mol1 to $last_B_mol1 and name CA)"
  set ref [atomselect $mol1 $selection1 frame 0]

  set all2 [atomselect $mol2 all]
  set selection2 "(chain A and resid 1 to 186 and name CA) or (chain B and resid $start_B_mol2 to $last_B_mol2 and name CA)"
  set sel2 [atomselect $mol2 $selection2]
  
  set numframes [molinfo $mol1 get numframes]

  for {set i 0} {$i < $numframes} {incr i} {

    $all2 frame $i
    $sel2 frame $i
    $all2 move [measure fit $sel2 $ref]
  }
  
  

} else {

  set ref [atomselect $mol1 "name CA" frame 0]
  set all2 [atomselect $mol2 all]
  set sel2 [atomselect $mol2 "name CA"]


  set numframes [molinfo $mol1 get numframes]

  for {set i 0} {$i < $numframes} {incr i} {

    $all2 frame $i
    $sel2 frame $i
    $all2 move [measure fit $sel2 $ref]
  }

}

animate write dcd /tmp/$out.dcd waitfor all  $mol2

$all2 frame 0
$all2 writepdb /tmp/$out.pdb

$sel2 delete
$all2 delete

exit
